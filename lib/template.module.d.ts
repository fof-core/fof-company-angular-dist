import { ModuleWithProviders } from '@angular/core';
import { IfofConfig } from '@fof-angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/platform-browser";
import * as i2 from "./material.module";
import * as i3 from "./settings/settings.module";
import * as i4 from "./layout/layout.module";
import * as i5 from "@angular/common/http";
import * as i6 from "@fof-angular/core";
import * as i7 from "@angular/platform-browser/animations";
export declare class TemplateModule {
    static forRoot(fofConfig: IfofConfig): ModuleWithProviders<TemplateModule>;
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<TemplateModule, never, [typeof i1.BrowserModule, typeof i2.MaterialModule, typeof i3.SettingsModule, typeof i4.LayoutModule, typeof i5.HttpClientModule, typeof i6.AdminModule], [typeof i7.BrowserAnimationsModule, typeof i4.LayoutModule]>;
    static ɵinj: i0.ɵɵInjectorDef<TemplateModule>;
}
