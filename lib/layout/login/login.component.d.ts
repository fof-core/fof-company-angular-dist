import { OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FoFAuthService, FofNotificationService, FofErrorService } from '@fof-angular/core';
import * as i0 from "@angular/core";
export declare class LoginComponent implements OnInit {
    private foFAuthService;
    private activatedRoute;
    private router;
    private fofNotificationService;
    private fofErrorService;
    constructor(foFAuthService: FoFAuthService, activatedRoute: ActivatedRoute, router: Router, fofNotificationService: FofNotificationService, fofErrorService: FofErrorService);
    private priVar;
    private privFunc;
    uiVar: {
        form: FormGroup;
        error: string;
        message: string;
        loading: boolean;
    };
    uiAction: {
        submit: () => void;
    };
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<LoginComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<LoginComponent, "company-login", never, {}, {}, never>;
}
