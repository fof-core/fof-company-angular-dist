import { OnInit } from '@angular/core';
import { FoFAuthService, FofNotificationService, FofErrorService } from '@fof-angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as i0 from "@angular/core";
export declare class LoadingPageComponent implements OnInit {
    private foFAuthService;
    private activatedRoute;
    private router;
    private fofNotificationService;
    private fofErrorService;
    constructor(foFAuthService: FoFAuthService, activatedRoute: ActivatedRoute, router: Router, fofNotificationService: FofNotificationService, fofErrorService: FofErrorService);
    private priVar;
    private privFunc;
    uiVar: {
        message: string;
    };
    uiAction: {};
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<LoadingPageComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<LoadingPageComponent, "company-loading-page", never, {}, {}, never>;
}
