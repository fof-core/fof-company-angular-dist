import * as i0 from "@angular/core";
import * as i1 from "./login/login.component";
import * as i2 from "./not-found/not-found.component";
import * as i3 from "./master-page/master-page.component";
import * as i4 from "./loading-page/loading-page.component";
import * as i5 from "@angular/common";
import * as i6 from "./layout-routing.module";
import * as i7 from "../material.module";
import * as i8 from "@fof-angular/core";
import * as i9 from "@angular/forms";
export declare class LayoutModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<LayoutModule, [typeof i1.LoginComponent, typeof i2.NotFoundComponent, typeof i3.MasterPageComponent, typeof i4.LoadingPageComponent], [typeof i5.CommonModule, typeof i6.LayoutRoutingModule, typeof i7.MaterialModule, typeof i8.FoFCoreModule, typeof i9.ReactiveFormsModule], [typeof i3.MasterPageComponent, typeof i2.NotFoundComponent, typeof i8.FoFCoreModule]>;
    static ɵinj: i0.ɵɵInjectorDef<LayoutModule>;
}
