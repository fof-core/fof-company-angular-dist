import { OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { OverlayContainer } from '@angular/cdk/overlay';
import { SettingsService } from '../../settings/settings.service';
import { FoFAuthService, iUser, FofNotificationService, IfofConfig, IfofNavigation } from '@fof-angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as i0 from "@angular/core";
export declare class MasterPageComponent implements OnInit {
    private overlayContainer;
    private readonly settingsService;
    private fofConfig;
    private readonly foFAuthService;
    private fofNotificationService;
    private translateService;
    constructor(overlayContainer: OverlayContainer, settingsService: SettingsService, fofConfig: IfofConfig, foFAuthService: FoFAuthService, fofNotificationService: FofNotificationService, translateService: TranslateService);
    private priVar;
    private privFunc;
    uiVar: {
        stickyHeader$: Observable<boolean>;
        pageTransition: boolean;
        theme$: Observable<string>;
        fofConfig: IfofConfig;
        navigationMain: IfofNavigation[];
        currentUser: iUser;
        currentUserShortName: string;
        loading: boolean;
        backendPath: string;
    };
    uiAction: {
        logOut: () => void;
        getRouterOutletState: (outlet: any) => any;
    };
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<MasterPageComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<MasterPageComponent, "company-master-page", never, {}, {}, never>;
}
