import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/card";
import * as i3 from "@angular/material/divider";
import * as i4 from "@angular/material/icon";
import * as i5 from "@angular/material/input";
import * as i6 from "@angular/material/menu";
import * as i7 from "@angular/material/progress-spinner";
import * as i8 from "@angular/material/select";
import * as i9 from "@angular/material/sidenav";
import * as i10 from "@angular/material/slide-toggle";
import * as i11 from "@angular/material/toolbar";
export declare class MaterialModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MaterialModule, never, [typeof i1.MatButtonModule, typeof i2.MatCardModule, typeof i3.MatDividerModule, typeof i4.MatIconModule, typeof i5.MatInputModule, typeof i6.MatMenuModule, typeof i7.MatProgressSpinnerModule, typeof i8.MatSelectModule, typeof i9.MatSidenavModule, typeof i10.MatSlideToggleModule, typeof i11.MatToolbarModule], [typeof i1.MatButtonModule, typeof i2.MatCardModule, typeof i3.MatDividerModule, typeof i4.MatIconModule, typeof i5.MatInputModule, typeof i6.MatMenuModule, typeof i7.MatProgressSpinnerModule, typeof i8.MatSelectModule, typeof i9.MatSidenavModule, typeof i10.MatSlideToggleModule, typeof i11.MatToolbarModule]>;
    static ɵinj: i0.ɵɵInjectorDef<MaterialModule>;
}
