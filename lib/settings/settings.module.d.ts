import * as i0 from "@angular/core";
import * as i1 from "./settings-config/settings-config.component";
import * as i2 from "@angular/common";
import * as i3 from "./settings-routing.module";
import * as i4 from "@angular/forms";
import * as i5 from "../material.module";
export declare class SettingsModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<SettingsModule, [typeof i1.SettingsConfigComponent], [typeof i2.CommonModule, typeof i3.SettingsRoutingModule, typeof i4.FormsModule, typeof i5.MaterialModule], never>;
    static ɵinj: i0.ɵɵInjectorDef<SettingsModule>;
}
