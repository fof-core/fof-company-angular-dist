import { OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';
import * as i0 from "@angular/core";
export declare class SettingsConfigComponent implements OnInit {
    private readonly settingsService;
    constructor(settingsService: SettingsService);
    private priVar;
    private privFunc;
    uiVar: {
        themes: {
            value: string;
            label: string;
        }[];
        currentTheme: string;
        currentStickyHeader: boolean;
        currentPageTransition: boolean;
    };
    uiAction: {
        onThemeSelect: (value: string) => void;
        onStickyHeaderToggle: (value: boolean) => void;
        onPageTransitionToggle: (value: boolean) => void;
    };
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<SettingsConfigComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<SettingsConfigComponent, "company-settings-config", never, {}, {}, never>;
}
