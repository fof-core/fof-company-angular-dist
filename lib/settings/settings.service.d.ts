import { Observable } from 'rxjs';
import { FofLocalstorageService } from '@fof-angular/core';
import * as i0 from "@angular/core";
export declare class SettingsService {
    private fofLocalstorageService;
    constructor(fofLocalstorageService: FofLocalstorageService);
    private _theme;
    private _themeChange;
    readonly themeChange: Observable<string>;
    get theme(): string;
    set theme(theme: string);
    private _stickyHeader;
    private _stickyHeaderChange;
    readonly stickyHeaderChange: Observable<boolean>;
    get stickyHeader(): boolean;
    set stickyHeader(stickyHeader: boolean);
    private _pageTransition;
    private _pageTransitionChange;
    get pageTransitionChange(): Observable<boolean>;
    get pageTransition(): boolean;
    set pageTransition(pageTransition: boolean);
    static ɵfac: i0.ɵɵFactoryDef<SettingsService>;
    static ɵprov: i0.ɵɵInjectableDef<SettingsService>;
}
