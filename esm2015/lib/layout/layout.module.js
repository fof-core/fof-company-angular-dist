import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { LayoutRoutingModule } from './layout-routing.module';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MasterPageComponent } from './master-page/master-page.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';
import { FoFCoreModule } from '@fof-angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { frenchPaginatorIntl } from './french-paginator-intl';
import { MatPaginatorIntl } from '@angular/material/paginator';
import * as i0 from "@angular/core";
export class LayoutModule {
}
LayoutModule.ɵmod = i0.ɵɵdefineNgModule({ type: LayoutModule });
LayoutModule.ɵinj = i0.ɵɵdefineInjector({ factory: function LayoutModule_Factory(t) { return new (t || LayoutModule)(); }, providers: [
        { provide: MatPaginatorIntl, useValue: frenchPaginatorIntl() }
    ], imports: [[
            CommonModule,
            LayoutRoutingModule,
            MaterialModule,
            FoFCoreModule,
            ReactiveFormsModule
        ],
        FoFCoreModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(LayoutModule, { declarations: [LoginComponent,
        NotFoundComponent,
        MasterPageComponent,
        LoadingPageComponent], imports: [CommonModule,
        LayoutRoutingModule,
        MaterialModule,
        FoFCoreModule,
        ReactiveFormsModule], exports: [MasterPageComponent,
        NotFoundComponent,
        FoFCoreModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LayoutModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    LoginComponent,
                    NotFoundComponent,
                    MasterPageComponent,
                    LoadingPageComponent
                ],
                imports: [
                    CommonModule,
                    LayoutRoutingModule,
                    MaterialModule,
                    FoFCoreModule,
                    ReactiveFormsModule
                ],
                providers: [
                    { provide: MatPaginatorIntl, useValue: frenchPaginatorIntl() }
                ],
                exports: [
                    MasterPageComponent,
                    NotFoundComponent,
                    FoFCoreModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xheW91dC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBdUIsTUFBTSxlQUFlLENBQUE7QUFDN0QsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQTtBQUNuRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUM3RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUE7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUE7QUFDbkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0scUNBQXFDLENBQUE7QUFDekUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUNBQXVDLENBQUE7QUFDNUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFBO0FBQ2pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBQ3BELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFBO0FBQzdELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDZCQUE2QixDQUFBOztBQTBCOUQsTUFBTSxPQUFPLFlBQVk7O2dEQUFaLFlBQVk7dUdBQVosWUFBWSxtQkFUWjtRQUNULEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxtQkFBbUIsRUFBRSxFQUFFO0tBQy9ELFlBVFE7WUFDUCxZQUFZO1lBQ1osbUJBQW1CO1lBQ25CLGNBQWM7WUFDZCxhQUFhO1lBQ2IsbUJBQW1CO1NBQ3BCO1FBT0MsYUFBYTt3RkFHSixZQUFZLG1CQXJCckIsY0FBYztRQUNkLGlCQUFpQjtRQUNqQixtQkFBbUI7UUFDbkIsb0JBQW9CLGFBR3BCLFlBQVk7UUFDWixtQkFBbUI7UUFDbkIsY0FBYztRQUNkLGFBQWE7UUFDYixtQkFBbUIsYUFNbkIsbUJBQW1CO1FBQ25CLGlCQUFpQjtRQUNqQixhQUFhO2tEQUdKLFlBQVk7Y0F2QnhCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUU7b0JBQ1osY0FBYztvQkFDZCxpQkFBaUI7b0JBQ2pCLG1CQUFtQjtvQkFDbkIsb0JBQW9CO2lCQUNyQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2QsYUFBYTtvQkFDYixtQkFBbUI7aUJBQ3BCO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsbUJBQW1CLEVBQUUsRUFBRTtpQkFDL0Q7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLG1CQUFtQjtvQkFDbkIsaUJBQWlCO29CQUNqQixhQUFhO2lCQUNkO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSdcbmltcG9ydCB7IExheW91dFJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL2xheW91dC1yb3V0aW5nLm1vZHVsZSdcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9sb2dpbi9sb2dpbi5jb21wb25lbnQnXG5pbXBvcnQgeyBOb3RGb3VuZENvbXBvbmVudCB9IGZyb20gJy4vbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQnXG5pbXBvcnQgeyBNYXN0ZXJQYWdlQ29tcG9uZW50IH0gZnJvbSAnLi9tYXN0ZXItcGFnZS9tYXN0ZXItcGFnZS5jb21wb25lbnQnXG5pbXBvcnQgeyBMb2FkaW5nUGFnZUNvbXBvbmVudCB9IGZyb20gJy4vbG9hZGluZy1wYWdlL2xvYWRpbmctcGFnZS5jb21wb25lbnQnXG5pbXBvcnQgeyBGb0ZDb3JlTW9kdWxlIH0gZnJvbSAnQGZvZi1hbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnXG5pbXBvcnQgeyBmcmVuY2hQYWdpbmF0b3JJbnRsIH0gZnJvbSAnLi9mcmVuY2gtcGFnaW5hdG9yLWludGwnXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3JJbnRsIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJ1xuXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIExvZ2luQ29tcG9uZW50LCBcbiAgICBOb3RGb3VuZENvbXBvbmVudCwgXG4gICAgTWFzdGVyUGFnZUNvbXBvbmVudCwgXG4gICAgTG9hZGluZ1BhZ2VDb21wb25lbnQgICAgXG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgTGF5b3V0Um91dGluZ01vZHVsZSxcbiAgICBNYXRlcmlhbE1vZHVsZSxcbiAgICBGb0ZDb3JlTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgeyBwcm92aWRlOiBNYXRQYWdpbmF0b3JJbnRsLCB1c2VWYWx1ZTogZnJlbmNoUGFnaW5hdG9ySW50bCgpIH1cbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIE1hc3RlclBhZ2VDb21wb25lbnQsXG4gICAgTm90Rm91bmRDb21wb25lbnQsXG4gICAgRm9GQ29yZU1vZHVsZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIExheW91dE1vZHVsZSB7fVxuIl19