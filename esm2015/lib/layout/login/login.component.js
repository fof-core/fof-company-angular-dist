import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@fof-angular/core";
import * as i2 from "@angular/router";
import * as i3 from "@angular/material/card";
import * as i4 from "@angular/common";
import * as i5 from "@angular/forms";
import * as i6 from "@angular/material/form-field";
import * as i7 from "@angular/material/input";
import * as i8 from "@angular/material/button";
import * as i9 from "@angular/material/progress-spinner";
function LoginComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelement(1, "mat-spinner", 11);
    i0.ɵɵelementEnd();
} }
function LoginComponent_p_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "p", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r3.uiVar.error, " ");
} }
function LoginComponent_p_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "p", 13);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r4.uiVar.message, " ");
} }
export class LoginComponent {
    constructor(foFAuthService, activatedRoute, router, fofNotificationService, fofErrorService) {
        this.foFAuthService = foFAuthService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            returnUrl: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            form: new FormGroup({
                username: new FormControl('superuser@fof.com'),
                password: new FormControl('changeMe')
            }),
            error: undefined,
            message: undefined,
            loading: false
        };
        // All actions shared with UI 
        this.uiAction = {
            submit: () => {
                if (!this.uiVar.form.valid) {
                    return;
                }
                const username = this.uiVar.form.value.username;
                const password = this.uiVar.form.value.password;
                this.uiVar.loading = true;
                this.foFAuthService.login(username, password)
                    .toPromise()
                    .then(data => {
                    this.router.navigate([this.priVar.returnUrl]);
                })
                    .catch(reason => {
                    // this.uiVar.error = error
                    this.fofErrorService.errorManage(reason);
                    this.uiVar.loading = false;
                });
            }
        };
        this.priVar.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
        // try to get the user identity with the cookie
        this.uiVar.message = "Nous cherchons si vous nous connaissons...";
        this.foFAuthService.refreshByCookie()
            .toPromise()
            .then((user) => {
            if (user) {
                this.fofNotificationService.info(`Bonjour ${user.firstName} !`);
                this.router.navigate([this.priVar.returnUrl]);
            }
        })
            .catch(reason => {
            // this.uiVar.error = reason.message
            this.fofErrorService.errorManage(reason);
            this.uiVar.loading = false;
        })
            .finally(() => { this.uiVar.message = 'Veuillez vous authentifier'; });
    }
    // Angular events  
    ngOnInit() {
    }
    ngOnDestroy() {
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(i0.ɵɵdirectiveInject(i1.FoFAuthService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i1.FofNotificationService), i0.ɵɵdirectiveInject(i1.FofErrorService)); };
LoginComponent.ɵcmp = i0.ɵɵdefineComponent({ type: LoginComponent, selectors: [["company-login"]], decls: 19, vars: 4, consts: [[1, "background"], [1, "main"], ["class", "fof-loading", 4, "ngIf"], [3, "formGroup", "ngSubmit"], ["type", "text", "matInput", "", "placeholder", "Username", "formControlName", "username"], ["type", "password", "matInput", "", "placeholder", "Password", "formControlName", "password"], ["class", "error", 4, "ngIf"], ["class", "message", 4, "ngIf"], [1, "button"], ["type", "submit", "mat-button", ""], [1, "fof-loading"], ["diameter", "30"], [1, "error"], [1, "message"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "mat-card");
        i0.ɵɵtemplate(3, LoginComponent_div_3_Template, 2, 0, "div", 2);
        i0.ɵɵelementStart(4, "mat-card-title");
        i0.ɵɵtext(5, " Login ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "mat-card-content");
        i0.ɵɵelementStart(7, "form", 3);
        i0.ɵɵlistener("ngSubmit", function LoginComponent_Template_form_ngSubmit_7_listener() { return ctx.uiAction.submit(); });
        i0.ɵɵelementStart(8, "p");
        i0.ɵɵelementStart(9, "mat-form-field");
        i0.ɵɵelement(10, "input", 4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "p");
        i0.ɵɵelementStart(12, "mat-form-field");
        i0.ɵɵelement(13, "input", 5);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(14, LoginComponent_p_14_Template, 2, 1, "p", 6);
        i0.ɵɵtemplate(15, LoginComponent_p_15_Template, 2, 1, "p", 7);
        i0.ɵɵelementStart(16, "div", 8);
        i0.ɵɵelementStart(17, "button", 9);
        i0.ɵɵtext(18, "Login");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("formGroup", ctx.uiVar.form);
        i0.ɵɵadvance(7);
        i0.ɵɵproperty("ngIf", ctx.uiVar.error);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.message);
    } }, directives: [i3.MatCard, i4.NgIf, i3.MatCardTitle, i3.MatCardContent, i5.ɵangular_packages_forms_forms_y, i5.NgControlStatusGroup, i5.FormGroupDirective, i6.MatFormField, i7.MatInput, i5.DefaultValueAccessor, i5.NgControlStatus, i5.FormControlName, i8.MatButton, i9.MatSpinner], styles: [".background[_ngcontent-%COMP%]{position:absolute;top:0;left:0;right:0;bottom:0;z-index:1000}.main[_ngcontent-%COMP%]{display:flex;justify-content:center;margin:100px 0}.main[_ngcontent-%COMP%]   .fof-loading[_ngcontent-%COMP%]{display:flex;justify-content:center;align-items:center;position:absolute;top:0;left:0;width:100%;height:100%;z-index:2}.main[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%;min-width:300px}.main[_ngcontent-%COMP%]   mat-card-content[_ngcontent-%COMP%], .main[_ngcontent-%COMP%]   mat-card-title[_ngcontent-%COMP%]{display:flex;justify-content:center}.main[_ngcontent-%COMP%]   .error[_ngcontent-%COMP%], .main[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]{padding:16px;width:300px}.main[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]{display:flex;justify-content:flex-end}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LoginComponent, [{
        type: Component,
        args: [{
                selector: 'company-login',
                templateUrl: './login.component.html',
                styleUrls: ['./login.component.scss']
            }]
    }], function () { return [{ type: i1.FoFAuthService }, { type: i2.ActivatedRoute }, { type: i2.Router }, { type: i1.FofNotificationService }, { type: i1.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbG9naW4vbG9naW4uY29tcG9uZW50LnRzIiwibGliL2xheW91dC9sb2dpbi9sb2dpbi5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFBO0FBQ2pELE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7Ozs7Ozs7Ozs7OztJQ0VqRCwrQkFDRTtJQUFBLGtDQUF1QztJQUN6QyxpQkFBTTs7O0lBZ0JGLDZCQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFJOzs7SUFERixlQUNGO0lBREUsbURBQ0Y7OztJQUNBLDZCQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFJOzs7SUFERixlQUNGO0lBREUscURBQ0Y7O0FEaEJWLE1BQU0sT0FBTyxjQUFjO0lBRXpCLFlBQ1UsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsTUFBYyxFQUNkLHNCQUE4QyxFQUM5QyxlQUFnQztRQUpoQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQXdCMUMsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLFNBQVMsRUFBVSxTQUFTO1NBQzdCLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsSUFBSSxFQUFjLElBQUksU0FBUyxDQUFDO2dCQUM5QixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsbUJBQW1CLENBQUM7Z0JBQzlDLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUM7YUFDdEMsQ0FBQztZQUNGLEtBQUssRUFBVSxTQUFTO1lBQ3hCLE9BQU8sRUFBVSxTQUFTO1lBQzFCLE9BQU8sRUFBRSxLQUFLO1NBQ2YsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsTUFBTSxFQUFDLEdBQUcsRUFBRTtnQkFFVixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUMxQixPQUFNO2lCQUNQO2dCQUVELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUE7Z0JBQy9DLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUE7Z0JBRS9DLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQTtnQkFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQztxQkFDNUMsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQTtnQkFDL0MsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDZCwyQkFBMkI7b0JBQzNCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7Z0JBQzVCLENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQztTQUNGLENBQUE7UUE3REMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsQ0FBQTtRQUVwRiwrQ0FBK0M7UUFDL0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsNENBQTRDLENBQUE7UUFDakUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUU7YUFDcEMsU0FBUyxFQUFFO2FBQ1gsSUFBSSxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDbEIsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFBO2dCQUMvRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQTthQUM5QztRQUNILENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNkLG9DQUFvQztZQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7UUFDNUIsQ0FBQyxDQUFDO2FBQ0QsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLDRCQUE0QixDQUFBLENBQUEsQ0FBQyxDQUFDLENBQUE7SUFDeEUsQ0FBQztJQTRDQyxtQkFBbUI7SUFDbkIsUUFBUTtJQUNSLENBQUM7SUFFRCxXQUFXO0lBQ1gsQ0FBQzs7NEVBN0VVLGNBQWM7bURBQWQsY0FBYztRQ1YzQiw4QkFDRTtRQUFBLDhCQUNFO1FBQUEsZ0NBQ0U7UUFBQSwrREFDRTtRQUVGLHNDQUNFO1FBQUEsdUJBQ0Y7UUFBQSxpQkFBaUI7UUFDakIsd0NBQ0U7UUFBQSwrQkFDRTtRQUQ2QiwrRkFBWSxxQkFBaUIsSUFBQztRQUMzRCx5QkFDRTtRQUFBLHNDQUNFO1FBQUEsNEJBQ0Y7UUFBQSxpQkFBaUI7UUFDbkIsaUJBQUk7UUFDSiwwQkFDRTtRQUFBLHVDQUNFO1FBQUEsNEJBQ0Y7UUFBQSxpQkFBaUI7UUFDbkIsaUJBQUk7UUFDSiw2REFDRTtRQUVGLDZEQUNFO1FBRUYsK0JBQ0U7UUFBQSxrQ0FBaUM7UUFBQSxzQkFBSztRQUFBLGlCQUFTO1FBQ2pELGlCQUFNO1FBQ1IsaUJBQU87UUFDVCxpQkFBbUI7UUFDckIsaUJBQVc7UUFDYixpQkFBTTtRQUNSLGlCQUFNOztRQS9CSyxlQUFxQjtRQUFyQix3Q0FBcUI7UUFPbEIsZUFBd0I7UUFBeEIsMENBQXdCO1FBV3pCLGVBQW1CO1FBQW5CLHNDQUFtQjtRQUduQixlQUFxQjtRQUFyQix3Q0FBcUI7O2tERGRyQixjQUFjO2NBTDFCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsV0FBVyxFQUFFLHdCQUF3QjtnQkFDckMsU0FBUyxFQUFFLENBQUMsd0JBQXdCLENBQUM7YUFDdEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UsIEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsIEZvZkVycm9yU2VydmljZSB9IGZyb20gJ0Bmb2YtYW5ndWxhci9jb3JlJ1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb21wYW55LWxvZ2luJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbG9naW4uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBmb0ZBdXRoU2VydmljZTogRm9GQXV0aFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmb2ZFcnJvclNlcnZpY2U6IEZvZkVycm9yU2VydmljZVxuICApIHsgXG5cbiAgICB0aGlzLnByaVZhci5yZXR1cm5VcmwgPSB0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90LnF1ZXJ5UGFyYW1zWydyZXR1cm5VcmwnXSB8fCAnLydcblxuICAgIC8vIHRyeSB0byBnZXQgdGhlIHVzZXIgaWRlbnRpdHkgd2l0aCB0aGUgY29va2llXG4gICAgdGhpcy51aVZhci5tZXNzYWdlID0gXCJOb3VzIGNoZXJjaG9ucyBzaSB2b3VzIG5vdXMgY29ubmFpc3NvbnMuLi5cIiAgICBcbiAgICB0aGlzLmZvRkF1dGhTZXJ2aWNlLnJlZnJlc2hCeUNvb2tpZSgpXG4gICAgLnRvUHJvbWlzZSgpXG4gICAgLnRoZW4oKHVzZXI6IGFueSkgPT4geyAgICAgIFxuICAgICAgaWYgKHVzZXIpIHtcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmluZm8oYEJvbmpvdXIgJHt1c2VyLmZpcnN0TmFtZX0gIWApXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLnByaVZhci5yZXR1cm5VcmxdKVxuICAgICAgfSAgICAgIFxuICAgIH0pXG4gICAgLmNhdGNoKHJlYXNvbiA9PiB7ICAgICAgXG4gICAgICAvLyB0aGlzLnVpVmFyLmVycm9yID0gcmVhc29uLm1lc3NhZ2VcbiAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcbiAgICAgIHRoaXMudWlWYXIubG9hZGluZyA9IGZhbHNlXG4gICAgfSlcbiAgICAuZmluYWxseSgoKSA9PiB7IHRoaXMudWlWYXIubWVzc2FnZSA9ICdWZXVpbGxleiB2b3VzIGF1dGhlbnRpZmllcid9KVxufVxuIFxuXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xuICBwcml2YXRlIHByaVZhciA9IHtcbiAgICByZXR1cm5Vcmw6IDxzdHJpbmc+dW5kZWZpbmVkICAgIFxuICB9XG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xuICBwcml2YXRlIHByaXZGdW5jID0ge1xuICB9XG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXG4gIHB1YmxpYyB1aVZhciA9IHtcbiAgICBmb3JtOiA8Rm9ybUdyb3VwPiBuZXcgRm9ybUdyb3VwKHtcbiAgICAgIHVzZXJuYW1lOiBuZXcgRm9ybUNvbnRyb2woJ3N1cGVydXNlckBmb2YuY29tJyksXG4gICAgICBwYXNzd29yZDogbmV3IEZvcm1Db250cm9sKCdjaGFuZ2VNZScpXG4gICAgfSksXG4gICAgZXJyb3I6IDxzdHJpbmc+dW5kZWZpbmVkLFxuICAgIG1lc3NhZ2U6IDxzdHJpbmc+dW5kZWZpbmVkLFxuICAgIGxvYWRpbmc6IGZhbHNlXG4gIH1cbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcbiAgICBzdWJtaXQ6KCkgPT4ge1xuXG4gICAgICBpZiAoIXRoaXMudWlWYXIuZm9ybS52YWxpZCkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgY29uc3QgdXNlcm5hbWUgPSB0aGlzLnVpVmFyLmZvcm0udmFsdWUudXNlcm5hbWVcbiAgICAgIGNvbnN0IHBhc3N3b3JkID0gdGhpcy51aVZhci5mb3JtLnZhbHVlLnBhc3N3b3JkXG5cbiAgICAgIHRoaXMudWlWYXIubG9hZGluZyA9IHRydWVcbiAgICAgIHRoaXMuZm9GQXV0aFNlcnZpY2UubG9naW4odXNlcm5hbWUsIHBhc3N3b3JkKVxuICAgICAgLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbihkYXRhID0+IHtcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucHJpVmFyLnJldHVyblVybF0pXG4gICAgICB9KSAgICAgICAgIFxuICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7ICAgICAgICBcbiAgICAgICAgLy8gdGhpcy51aVZhci5lcnJvciA9IGVycm9yXG4gICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcbiAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gZmFsc2VcbiAgICAgIH0pICAgICAgICAgICAgIFxuICAgIH1cbiAgfVxuICAvLyBBbmd1bGFyIGV2ZW50cyAgXG4gIG5nT25Jbml0KCkgeyAgIFxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7ICAgIFxuICB9XG59XG4iLCI8ZGl2IGNsYXNzPVwiYmFja2dyb3VuZFwiPlxuICA8ZGl2IGNsYXNzPVwibWFpblwiPlxuICAgIDxtYXQtY2FyZD5cbiAgICAgIDxkaXYgKm5nSWY9XCJ1aVZhci5sb2FkaW5nXCIgY2xhc3M9XCJmb2YtbG9hZGluZ1wiPlxuICAgICAgICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MzA+PC9tYXQtc3Bpbm5lcj5cbiAgICAgIDwvZGl2PlxuICAgICAgPG1hdC1jYXJkLXRpdGxlPlxuICAgICAgICBMb2dpblxuICAgICAgPC9tYXQtY2FyZC10aXRsZT5cbiAgICAgIDxtYXQtY2FyZC1jb250ZW50PlxuICAgICAgICA8Zm9ybSBbZm9ybUdyb3VwXT1cInVpVmFyLmZvcm1cIiAobmdTdWJtaXQpPVwidWlBY3Rpb24uc3VibWl0KClcIj5cbiAgICAgICAgICA8cD5cbiAgICAgICAgICAgIDxtYXQtZm9ybS1maWVsZD5cbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbWF0SW5wdXQgcGxhY2Vob2xkZXI9XCJVc2VybmFtZVwiIGZvcm1Db250cm9sTmFtZT1cInVzZXJuYW1lXCI+XG4gICAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuICAgICAgICAgIDwvcD4gICAgXG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XG4gICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBtYXRJbnB1dCBwbGFjZWhvbGRlcj1cIlBhc3N3b3JkXCIgZm9ybUNvbnRyb2xOYW1lPVwicGFzc3dvcmRcIj5cbiAgICAgICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG4gICAgICAgICAgPC9wPiAgICBcbiAgICAgICAgICA8cCAqbmdJZj1cInVpVmFyLmVycm9yXCIgY2xhc3M9XCJlcnJvclwiPlxuICAgICAgICAgICAge3sgdWlWYXIuZXJyb3IgfX1cbiAgICAgICAgICA8L3A+ICAgIFxuICAgICAgICAgIDxwICpuZ0lmPVwidWlWYXIubWVzc2FnZVwiIGNsYXNzPVwibWVzc2FnZVwiPlxuICAgICAgICAgICAge3sgdWlWYXIubWVzc2FnZSB9fVxuICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiYnV0dG9uXCI+XG4gICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBtYXQtYnV0dG9uPkxvZ2luPC9idXR0b24+XG4gICAgICAgICAgPC9kaXY+ICAgIFxuICAgICAgICA8L2Zvcm0+XG4gICAgICA8L21hdC1jYXJkLWNvbnRlbnQ+XG4gICAgPC9tYXQtY2FyZD5cbiAgPC9kaXY+XG48L2Rpdj5cblxuXG5cblxuIl19