import { Component } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@fof-angular/core";
import * as i2 from "@angular/router";
export class LoadingPageComponent {
    constructor(foFAuthService, activatedRoute, router, fofNotificationService, fofErrorService) {
        this.foFAuthService = foFAuthService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            returnUrl: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            message: `Veuillez patienter,<br>
                      Nous vérifions que vous nous connaissons...`
        };
        // All actions shared with UI 
        this.uiAction = {};
        this.priVar.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
        // try to get the user identity with the cookie    
        this.foFAuthService.refreshByCookie()
            .toPromise()
            .then((user) => {
            if (user) {
                return;
            }
            else {
                return this.foFAuthService.loginKerberos();
            }
        })
            .finally(() => {
            this.router.navigate([this.priVar.returnUrl]);
        })
            .catch(reason => {
            if (reason.status == 401) {
                let usereMail = reason.user.email;
                this.uiVar.message = `Vous n'êtes pas authorisé à vous connecter à cette application<br>
          Utilisateur: ${usereMail}`;
            }
            else {
                this.fofErrorService.errorManage(reason);
            }
        });
    }
    // Angular events
    ngOnInit() {
        // this.foFAuthService.loginKerberos()
        // .then(() => this.router.navigate([this.priVar.returnUrl]))
        // .catch((reason:iFofHttpException) => {
        //   if (reason.status==401) {
        //     let usereMail = reason.user.email
        //     this.uiVar.message = `Vous n'êtes pas authorisé à vous connecter à cette application<br>
        //       Utilisateur: ${usereMail}`
        //   } else {
        //     this.fofErrorService.errorManage(<any>reason)
        //   }      
        // })
    }
}
LoadingPageComponent.ɵfac = function LoadingPageComponent_Factory(t) { return new (t || LoadingPageComponent)(i0.ɵɵdirectiveInject(i1.FoFAuthService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i1.FofNotificationService), i0.ɵɵdirectiveInject(i1.FofErrorService)); };
LoadingPageComponent.ɵcmp = i0.ɵɵdefineComponent({ type: LoadingPageComponent, selectors: [["company-loading-page"]], decls: 2, vars: 1, consts: [[1, "background"], [1, "main", 3, "innerHTML"]], template: function LoadingPageComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelement(1, "div", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("innerHTML", ctx.uiVar.message, i0.ɵɵsanitizeHtml);
    } }, styles: [".background[_ngcontent-%COMP%]{position:absolute;top:0;left:0;right:0;bottom:0;z-index:1000;opacity:.3}.main[_ngcontent-%COMP%]{display:flex;justify-content:center;margin:100px 0}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LoadingPageComponent, [{
        type: Component,
        args: [{
                selector: 'company-loading-page',
                templateUrl: './loading-page.component.html',
                styleUrls: ['./loading-page.component.scss']
            }]
    }], function () { return [{ type: i1.FoFAuthService }, { type: i2.ActivatedRoute }, { type: i2.Router }, { type: i1.FofNotificationService }, { type: i1.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy1wYWdlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xvYWRpbmctcGFnZS9sb2FkaW5nLXBhZ2UuY29tcG9uZW50LnRzIiwibGliL2xheW91dC9sb2FkaW5nLXBhZ2UvbG9hZGluZy1wYWdlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUE7Ozs7QUFVakQsTUFBTSxPQUFPLG9CQUFvQjtJQUMvQixZQUNVLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLE1BQWMsRUFDZCxzQkFBOEMsRUFDOUMsZUFBZ0M7UUFKaEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUE0QjFDLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixTQUFTLEVBQVUsU0FBUztTQUM3QixDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLE9BQU8sRUFBVTtrRUFDNkM7U0FDL0QsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUcsRUFDakIsQ0FBQTtRQXhDQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxDQUFBO1FBRXBGLG1EQUFtRDtRQUNuRCxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTthQUNwQyxTQUFTLEVBQUU7YUFDWCxJQUFJLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTtZQUNsQixJQUFJLElBQUksRUFBRTtnQkFDUixPQUFNO2FBQ1A7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFBO2FBQzNDO1FBQ0gsQ0FBQyxDQUFDO2FBQ0QsT0FBTyxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBO1FBQy9DLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNkLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBRSxHQUFHLEVBQUU7Z0JBQ3RCLElBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO2dCQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRzt5QkFDSixTQUFTLEVBQUUsQ0FBQTthQUM3QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBTSxNQUFNLENBQUMsQ0FBQTthQUM5QztRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQWlCRCxpQkFBaUI7SUFDakIsUUFBUTtRQUNOLHNDQUFzQztRQUN0Qyw2REFBNkQ7UUFDN0QseUNBQXlDO1FBQ3pDLDhCQUE4QjtRQUM5Qix3Q0FBd0M7UUFDeEMsK0ZBQStGO1FBQy9GLG1DQUFtQztRQUNuQyxhQUFhO1FBQ2Isb0RBQW9EO1FBQ3BELFlBQVk7UUFFWixLQUFLO0lBQ1AsQ0FBQzs7d0ZBL0RVLG9CQUFvQjt5REFBcEIsb0JBQW9CO1FDVmpDLDhCQUNFO1FBQUEseUJBRU07UUFDUixpQkFBTTs7UUFGRixlQUEyQjtRQUEzQixnRUFBMkI7O2tERFFsQixvQkFBb0I7Y0FMaEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLFdBQVcsRUFBRSwrQkFBK0I7Z0JBQzVDLFNBQVMsRUFBRSxDQUFDLCtCQUErQixDQUFDO2FBQzdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UsIEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsIFxuICBGb2ZFcnJvclNlcnZpY2UsIGlGb2ZIdHRwRXhjZXB0aW9uIH0gZnJvbSAnQGZvZi1hbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb21wYW55LWxvYWRpbmctcGFnZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9sb2FkaW5nLXBhZ2UuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9sb2FkaW5nLXBhZ2UuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBMb2FkaW5nUGFnZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZm9GQXV0aFNlcnZpY2U6IEZvRkF1dGhTZXJ2aWNlLFxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2VcbiAgKSB7IFxuICAgIHRoaXMucHJpVmFyLnJldHVyblVybCA9IHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXNbJ3JldHVyblVybCddIHx8ICcvJ1xuXG4gICAgLy8gdHJ5IHRvIGdldCB0aGUgdXNlciBpZGVudGl0eSB3aXRoIHRoZSBjb29raWUgICAgXG4gICAgdGhpcy5mb0ZBdXRoU2VydmljZS5yZWZyZXNoQnlDb29raWUoKVxuICAgIC50b1Byb21pc2UoKVxuICAgIC50aGVuKCh1c2VyOiBhbnkpID0+IHsgICAgICBcbiAgICAgIGlmICh1c2VyKSB7ICAgICAgICBcbiAgICAgICAgcmV0dXJuXG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5mb0ZBdXRoU2VydmljZS5sb2dpbktlcmJlcm9zKCkgICAgICAgIFxuICAgICAgfVxuICAgIH0pXG4gICAgLmZpbmFsbHkoKCkgPT4ge1xuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucHJpVmFyLnJldHVyblVybF0pXG4gICAgfSlcbiAgICAuY2F0Y2gocmVhc29uID0+IHsgICAgXG4gICAgICBpZiAocmVhc29uLnN0YXR1cz09NDAxKSB7XG4gICAgICAgIGxldCB1c2VyZU1haWwgPSByZWFzb24udXNlci5lbWFpbFxuICAgICAgICB0aGlzLnVpVmFyLm1lc3NhZ2UgPSBgVm91cyBuJ8OqdGVzIHBhcyBhdXRob3Jpc8OpIMOgIHZvdXMgY29ubmVjdGVyIMOgIGNldHRlIGFwcGxpY2F0aW9uPGJyPlxuICAgICAgICAgIFV0aWxpc2F0ZXVyOiAke3VzZXJlTWFpbH1gXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZSg8YW55PnJlYXNvbilcbiAgICAgIH0gICAgXG4gICAgfSkgICAgXG4gIH1cbiAgXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xuICBwcml2YXRlIHByaVZhciA9IHtcbiAgICByZXR1cm5Vcmw6IDxzdHJpbmc+dW5kZWZpbmVkXG4gIH1cbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XG4gIH1cbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcbiAgcHVibGljIHVpVmFyID0ge1xuICAgIG1lc3NhZ2U6IDxzdHJpbmc+YFZldWlsbGV6IHBhdGllbnRlciw8YnI+XG4gICAgICAgICAgICAgICAgICAgICAgTm91cyB2w6lyaWZpb25zIHF1ZSB2b3VzIG5vdXMgY29ubmFpc3NvbnMuLi5gXG4gIH1cbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcbiAgfVxuICAvLyBBbmd1bGFyIGV2ZW50c1xuICBuZ09uSW5pdCgpIHtcbiAgICAvLyB0aGlzLmZvRkF1dGhTZXJ2aWNlLmxvZ2luS2VyYmVyb3MoKVxuICAgIC8vIC50aGVuKCgpID0+IHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLnByaVZhci5yZXR1cm5VcmxdKSlcbiAgICAvLyAuY2F0Y2goKHJlYXNvbjppRm9mSHR0cEV4Y2VwdGlvbikgPT4ge1xuICAgIC8vICAgaWYgKHJlYXNvbi5zdGF0dXM9PTQwMSkge1xuICAgIC8vICAgICBsZXQgdXNlcmVNYWlsID0gcmVhc29uLnVzZXIuZW1haWxcbiAgICAvLyAgICAgdGhpcy51aVZhci5tZXNzYWdlID0gYFZvdXMgbifDqnRlcyBwYXMgYXV0aG9yaXPDqSDDoCB2b3VzIGNvbm5lY3RlciDDoCBjZXR0ZSBhcHBsaWNhdGlvbjxicj5cbiAgICAvLyAgICAgICBVdGlsaXNhdGV1cjogJHt1c2VyZU1haWx9YFxuICAgIC8vICAgfSBlbHNlIHtcbiAgICAvLyAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UoPGFueT5yZWFzb24pXG4gICAgLy8gICB9ICAgICAgXG4gICAgICBcbiAgICAvLyB9KVxuICB9XG59XG4iLCI8ZGl2IGNsYXNzPVwiYmFja2dyb3VuZFwiPlxuICA8ZGl2IGNsYXNzPVwibWFpblwiXG4gICAgW2lubmVySFRNTF09XCJ1aVZhci5tZXNzYWdlXCI+ICAgICAgICBcbiAgPC9kaXY+XG48L2Rpdj4iXX0=