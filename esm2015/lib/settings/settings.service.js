import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@fof-angular/core";
export class SettingsService {
    constructor(fofLocalstorageService) {
        this.fofLocalstorageService = fofLocalstorageService;
        this._theme = undefined;
        this._themeChange = new BehaviorSubject(undefined);
        this.themeChange = this._themeChange.asObservable();
        this._stickyHeader = undefined;
        this._stickyHeaderChange = new BehaviorSubject(undefined);
        this.stickyHeaderChange = this._stickyHeaderChange.asObservable();
        this._pageTransition = undefined;
        this._pageTransitionChange = new BehaviorSubject(undefined);
        let theme = this.fofLocalstorageService.getItem('theme');
        if (theme) {
            this.theme = theme;
        }
        else {
            this.theme = 'light-theme';
        }
        let stickyHeader = this.fofLocalstorageService.getItem('stickyHeader');
        this.stickyHeader = stickyHeader;
        let pageTransition = this.fofLocalstorageService.getItem('pageTransition');
        this.pageTransition = pageTransition;
    }
    get theme() {
        return this._theme;
    }
    set theme(theme) {
        this._theme = theme;
        this._themeChange.next(this._theme);
        this.fofLocalstorageService.setItem('theme', this._theme);
    }
    get stickyHeader() {
        return this._stickyHeader;
    }
    set stickyHeader(stickyHeader) {
        this._stickyHeader = stickyHeader;
        this._stickyHeaderChange.next(this._stickyHeader);
        this.fofLocalstorageService.setItem('stickyHeader', this._stickyHeader);
    }
    get pageTransitionChange() {
        return this._pageTransitionChange.asObservable();
    }
    get pageTransition() {
        return this._pageTransition;
    }
    set pageTransition(pageTransition) {
        this._pageTransition = pageTransition;
        this._pageTransitionChange.next(this._pageTransition);
        this.fofLocalstorageService.setItem('pageTransition', this._pageTransition);
    }
}
SettingsService.ɵfac = function SettingsService_Factory(t) { return new (t || SettingsService)(i0.ɵɵinject(i1.FofLocalstorageService)); };
SettingsService.ɵprov = i0.ɵɵdefineInjectable({ token: SettingsService, factory: SettingsService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.FofLocalstorageService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3Muc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvc2V0dGluZ3Mvc2V0dGluZ3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQzFDLE9BQU8sRUFBRSxlQUFlLEVBQWMsTUFBTSxNQUFNLENBQUE7OztBQU9sRCxNQUFNLE9BQU8sZUFBZTtJQUUxQixZQUNVLHNCQUE4QztRQUE5QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBZWhELFdBQU0sR0FBVyxTQUFTLENBQUE7UUFDMUIsaUJBQVksR0FBNEIsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDOUQsZ0JBQVcsR0FBdUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQVcxRSxrQkFBYSxHQUFZLFNBQVMsQ0FBQTtRQUNsQyx3QkFBbUIsR0FBNkIsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDdEUsdUJBQWtCLEdBQXdCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQVd6RixvQkFBZSxHQUFZLFNBQVMsQ0FBQTtRQUNwQywwQkFBcUIsR0FBNkIsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUE7UUF4Q3RGLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDeEQsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQTtTQUNuQjthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUE7U0FDM0I7UUFDRCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3RFLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFBO1FBRWhDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUMxRSxJQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQTtJQUN0QyxDQUFDO0lBTUQsSUFBVyxLQUFLO1FBQ2QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFBO0lBQ3BCLENBQUM7SUFDRCxJQUFXLEtBQUssQ0FBQyxLQUFLO1FBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFBO1FBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUNuQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7SUFDM0QsQ0FBQztJQU1ELElBQVcsWUFBWTtRQUNyQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUE7SUFDM0IsQ0FBQztJQUNELElBQVcsWUFBWSxDQUFDLFlBQVk7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUE7UUFDakMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDakQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0lBQ3pFLENBQUM7SUFJRCxJQUFXLG9CQUFvQjtRQUM3QixPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtJQUNsRCxDQUFDO0lBRUQsSUFBVyxjQUFjO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQTtJQUM3QixDQUFDO0lBQ0QsSUFBVyxjQUFjLENBQUMsY0FBYztRQUN0QyxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQTtRQUNyQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUNyRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtJQUM3RSxDQUFDOzs4RUF6RFUsZUFBZTt1REFBZixlQUFlLFdBQWYsZUFBZSxtQkFGZCxNQUFNO2tEQUVQLGVBQWU7Y0FIM0IsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJ1xuaW1wb3J0IHsgRm9mTG9jYWxzdG9yYWdlU2VydmljZSB9IGZyb20gJ0Bmb2YtYW5ndWxhci9jb3JlJ1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFNldHRpbmdzU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBmb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlOiBGb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlXG4gICkgeyBcbiAgICBsZXQgdGhlbWUgPSB0aGlzLmZvZkxvY2Fsc3RvcmFnZVNlcnZpY2UuZ2V0SXRlbSgndGhlbWUnKVxuICAgIGlmICh0aGVtZSkge1xuICAgICAgdGhpcy50aGVtZSA9IHRoZW1lXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMudGhlbWUgPSAnbGlnaHQtdGhlbWUnXG4gICAgfVxuICAgIGxldCBzdGlja3lIZWFkZXIgPSB0aGlzLmZvZkxvY2Fsc3RvcmFnZVNlcnZpY2UuZ2V0SXRlbSgnc3RpY2t5SGVhZGVyJylcbiAgICB0aGlzLnN0aWNreUhlYWRlciA9IHN0aWNreUhlYWRlclxuICAgIFxuICAgIGxldCBwYWdlVHJhbnNpdGlvbiA9IHRoaXMuZm9mTG9jYWxzdG9yYWdlU2VydmljZS5nZXRJdGVtKCdwYWdlVHJhbnNpdGlvbicpXG4gICAgdGhpcy5wYWdlVHJhbnNpdGlvbiA9IHBhZ2VUcmFuc2l0aW9uXG4gIH1cblxuICBwcml2YXRlIF90aGVtZTogc3RyaW5nID0gdW5kZWZpbmVkXG4gIHByaXZhdGUgX3RoZW1lQ2hhbmdlOiBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPiA9IG5ldyBCZWhhdmlvclN1YmplY3QodW5kZWZpbmVkKSAgXG4gIHB1YmxpYyByZWFkb25seSB0aGVtZUNoYW5nZTogT2JzZXJ2YWJsZTxzdHJpbmc+ID0gdGhpcy5fdGhlbWVDaGFuZ2UuYXNPYnNlcnZhYmxlKClcblxuICBwdWJsaWMgZ2V0IHRoZW1lKCkge1xuICAgIHJldHVybiB0aGlzLl90aGVtZVxuICB9XG4gIHB1YmxpYyBzZXQgdGhlbWUodGhlbWUpIHtcbiAgICB0aGlzLl90aGVtZSA9IHRoZW1lXG4gICAgdGhpcy5fdGhlbWVDaGFuZ2UubmV4dCh0aGlzLl90aGVtZSlcbiAgICB0aGlzLmZvZkxvY2Fsc3RvcmFnZVNlcnZpY2Uuc2V0SXRlbSgndGhlbWUnLCB0aGlzLl90aGVtZSlcbiAgfVxuXG4gIHByaXZhdGUgX3N0aWNreUhlYWRlcjogYm9vbGVhbiA9IHVuZGVmaW5lZFxuICBwcml2YXRlIF9zdGlja3lIZWFkZXJDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPiA9IG5ldyBCZWhhdmlvclN1YmplY3QodW5kZWZpbmVkKSAgXG4gIHB1YmxpYyByZWFkb25seSBzdGlja3lIZWFkZXJDaGFuZ2U6IE9ic2VydmFibGU8Ym9vbGVhbj4gPSB0aGlzLl9zdGlja3lIZWFkZXJDaGFuZ2UuYXNPYnNlcnZhYmxlKClcblxuICBwdWJsaWMgZ2V0IHN0aWNreUhlYWRlcigpIHtcbiAgICByZXR1cm4gdGhpcy5fc3RpY2t5SGVhZGVyXG4gIH1cbiAgcHVibGljIHNldCBzdGlja3lIZWFkZXIoc3RpY2t5SGVhZGVyKSB7XG4gICAgdGhpcy5fc3RpY2t5SGVhZGVyID0gc3RpY2t5SGVhZGVyXG4gICAgdGhpcy5fc3RpY2t5SGVhZGVyQ2hhbmdlLm5leHQodGhpcy5fc3RpY2t5SGVhZGVyKVxuICAgIHRoaXMuZm9mTG9jYWxzdG9yYWdlU2VydmljZS5zZXRJdGVtKCdzdGlja3lIZWFkZXInLCB0aGlzLl9zdGlja3lIZWFkZXIpXG4gIH1cblxuICBwcml2YXRlIF9wYWdlVHJhbnNpdGlvbjogYm9vbGVhbiA9IHVuZGVmaW5lZFxuICBwcml2YXRlIF9wYWdlVHJhbnNpdGlvbkNoYW5nZTogQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+ID0gbmV3IEJlaGF2aW9yU3ViamVjdCh1bmRlZmluZWQpICBcbiAgcHVibGljIGdldCBwYWdlVHJhbnNpdGlvbkNoYW5nZSgpIHtcbiAgICByZXR1cm4gdGhpcy5fcGFnZVRyYW5zaXRpb25DaGFuZ2UuYXNPYnNlcnZhYmxlKClcbiAgfVxuXG4gIHB1YmxpYyBnZXQgcGFnZVRyYW5zaXRpb24oKSB7XG4gICAgcmV0dXJuIHRoaXMuX3BhZ2VUcmFuc2l0aW9uXG4gIH1cbiAgcHVibGljIHNldCBwYWdlVHJhbnNpdGlvbihwYWdlVHJhbnNpdGlvbikge1xuICAgIHRoaXMuX3BhZ2VUcmFuc2l0aW9uID0gcGFnZVRyYW5zaXRpb25cbiAgICB0aGlzLl9wYWdlVHJhbnNpdGlvbkNoYW5nZS5uZXh0KHRoaXMuX3BhZ2VUcmFuc2l0aW9uKVxuICAgIHRoaXMuZm9mTG9jYWxzdG9yYWdlU2VydmljZS5zZXRJdGVtKCdwYWdlVHJhbnNpdGlvbicsIHRoaXMuX3BhZ2VUcmFuc2l0aW9uKVxuICB9XG59XG4iXX0=