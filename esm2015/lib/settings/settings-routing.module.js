import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SettingsConfigComponent } from './settings-config/settings-config.component';
import { FofAuthGuard } from '@fof-angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
const routes = [
    { path: 'settings', component: SettingsConfigComponent,
        data: { title: 'Configuration' }, canActivate: [FofAuthGuard] }
];
export class SettingsRoutingModule {
}
SettingsRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: SettingsRoutingModule });
SettingsRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SettingsRoutingModule_Factory(t) { return new (t || SettingsRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
        RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SettingsRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3Mtcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29tcGFueS1hbmd1bGFyL3RlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL3NldHRpbmdzL3NldHRpbmdzLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFVLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQ3RELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFBO0FBQ3JGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQTs7O0FBRWhELE1BQU0sTUFBTSxHQUFXO0lBQ3JCLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsdUJBQXVCO1FBQ3BELElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsRUFBRSxXQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUMsRUFBQztDQUNqRSxDQUFBO0FBTUQsTUFBTSxPQUFPLHFCQUFxQjs7eURBQXJCLHFCQUFxQjt5SEFBckIscUJBQXFCLGtCQUh2QixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsWUFBWTt3RkFFWCxxQkFBcUIsMENBRnRCLFlBQVk7a0RBRVgscUJBQXFCO2NBSmpDLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBSb3V0ZXMsIFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcbmltcG9ydCB7IFNldHRpbmdzQ29uZmlnQ29tcG9uZW50IH0gZnJvbSAnLi9zZXR0aW5ncy1jb25maWcvc2V0dGluZ3MtY29uZmlnLmNvbXBvbmVudCdcbmltcG9ydCB7IEZvZkF1dGhHdWFyZCB9IGZyb20gJ0Bmb2YtYW5ndWxhci9jb3JlJ1xuXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcbiAgeyBwYXRoOiAnc2V0dGluZ3MnLCBjb21wb25lbnQ6IFNldHRpbmdzQ29uZmlnQ29tcG9uZW50LCBcbiAgICBkYXRhOiB7IHRpdGxlOiAnQ29uZmlndXJhdGlvbicgfSwgY2FuQWN0aXZhdGU6IFtGb2ZBdXRoR3VhcmRdfVxuXVxuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbUm91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxuICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBTZXR0aW5nc1JvdXRpbmdNb2R1bGUgeyB9XG4iXX0=