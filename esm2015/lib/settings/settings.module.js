import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsConfigComponent } from './settings-config/settings-config.component';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';
import * as i0 from "@angular/core";
export class SettingsModule {
}
SettingsModule.ɵmod = i0.ɵɵdefineNgModule({ type: SettingsModule });
SettingsModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SettingsModule_Factory(t) { return new (t || SettingsModule)(); }, imports: [[
            CommonModule,
            SettingsRoutingModule,
            FormsModule,
            MaterialModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SettingsModule, { declarations: [SettingsConfigComponent], imports: [CommonModule,
        SettingsRoutingModule,
        FormsModule,
        MaterialModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    SettingsConfigComponent
                ],
                imports: [
                    CommonModule,
                    SettingsRoutingModule,
                    FormsModule,
                    MaterialModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9zZXR0aW5ncy9zZXR0aW5ncy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUN4QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFFOUMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUE7QUFDakUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkNBQTZDLENBQUE7QUFFckYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFBO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7QUFhNUMsTUFBTSxPQUFPLGNBQWM7O2tEQUFkLGNBQWM7MkdBQWQsY0FBYyxrQkFQaEI7WUFDUCxZQUFZO1lBQ1oscUJBQXFCO1lBQ3JCLFdBQVc7WUFDWCxjQUFjO1NBQ2Y7d0ZBRVUsY0FBYyxtQkFUdkIsdUJBQXVCLGFBR3ZCLFlBQVk7UUFDWixxQkFBcUI7UUFDckIsV0FBVztRQUNYLGNBQWM7a0RBR0wsY0FBYztjQVgxQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLHVCQUF1QjtpQkFDeEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1oscUJBQXFCO29CQUNyQixXQUFXO29CQUNYLGNBQWM7aUJBQ2Y7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcblxuaW1wb3J0IHsgU2V0dGluZ3NSb3V0aW5nTW9kdWxlIH0gZnJvbSAnLi9zZXR0aW5ncy1yb3V0aW5nLm1vZHVsZSdcbmltcG9ydCB7IFNldHRpbmdzQ29uZmlnQ29tcG9uZW50IH0gZnJvbSAnLi9zZXR0aW5ncy1jb25maWcvc2V0dGluZ3MtY29uZmlnLmNvbXBvbmVudCdcblxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJ1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBTZXR0aW5nc0NvbmZpZ0NvbXBvbmVudFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFNldHRpbmdzUm91dGluZ01vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBNYXRlcmlhbE1vZHVsZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFNldHRpbmdzTW9kdWxlIHsgfVxuIl19