import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { SettingsModule } from './settings/settings.module';
import { CORE_CONFIG, AdminModule } from '@fof-angular/core';
import { LayoutModule } from './layout/layout.module';
import * as i0 from "@angular/core";
// import localeFr from '@angular/common/locales/fr'
// import { registerLocaleData } from '@angular/common'
// registerLocaleData(localeFr, 'fr')
export class TemplateModule {
    static forRoot(fofConfig) {
        return {
            ngModule: TemplateModule,
            providers: [
                {
                    provide: CORE_CONFIG,
                    useValue: fofConfig
                }
            ]
        };
    }
}
TemplateModule.ɵmod = i0.ɵɵdefineNgModule({ type: TemplateModule });
TemplateModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TemplateModule_Factory(t) { return new (t || TemplateModule)(); }, imports: [[
            BrowserModule,
            MaterialModule,
            SettingsModule,
            LayoutModule,
            HttpClientModule,
            AdminModule
        ],
        BrowserAnimationsModule,
        LayoutModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TemplateModule, { imports: [BrowserModule,
        MaterialModule,
        SettingsModule,
        LayoutModule,
        HttpClientModule,
        AdminModule], exports: [BrowserAnimationsModule,
        LayoutModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TemplateModule, [{
        type: NgModule,
        args: [{
                declarations: [],
                imports: [
                    BrowserModule,
                    MaterialModule,
                    SettingsModule,
                    LayoutModule,
                    HttpClientModule,
                    AdminModule
                ],
                // providers: [
                //   {provide: LOCALE_ID, useValue: 'fr' },
                // ],
                exports: [
                    BrowserAnimationsModule,
                    LayoutModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi90ZW1wbGF0ZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBa0MsTUFBTSxlQUFlLENBQUE7QUFDeEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUE7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDJCQUEyQixDQUFBO0FBQ3pELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFBO0FBQzlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTyxtQkFBbUIsQ0FBQTtBQUNuRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUE7QUFDM0QsT0FBTyxFQUFjLFdBQVcsRUFBRSxXQUFXLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQTtBQUN4RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7O0FBQ3JELG9EQUFvRDtBQUNwRCx1REFBdUQ7QUFFdkQscUNBQXFDO0FBc0JyQyxNQUFNLE9BQU8sY0FBYztJQUN6QixNQUFNLENBQUMsT0FBTyxDQUFDLFNBQXFCO1FBQ2xDLE9BQU87WUFDTCxRQUFRLEVBQUUsY0FBYztZQUN4QixTQUFTLEVBQUU7Z0JBQ1Q7b0JBQ0UsT0FBTyxFQUFFLFdBQVc7b0JBQ3BCLFFBQVEsRUFBRSxTQUFTO2lCQUNwQjthQUNGO1NBQ0YsQ0FBQTtJQUNILENBQUM7O2tEQVhVLGNBQWM7MkdBQWQsY0FBYyxrQkFoQmhCO1lBQ1AsYUFBYTtZQUNiLGNBQWM7WUFDZCxjQUFjO1lBQ2QsWUFBWTtZQUNaLGdCQUFnQjtZQUNoQixXQUFXO1NBQ1o7UUFLQyx1QkFBdUI7UUFDdkIsWUFBWTt3RkFHSCxjQUFjLGNBZnZCLGFBQWE7UUFDYixjQUFjO1FBQ2QsY0FBYztRQUNkLFlBQVk7UUFDWixnQkFBZ0I7UUFDaEIsV0FBVyxhQU1YLHVCQUF1QjtRQUN2QixZQUFZO2tEQUdILGNBQWM7Y0FwQjFCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsRUFFYjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsYUFBYTtvQkFDYixjQUFjO29CQUNkLGNBQWM7b0JBQ2QsWUFBWTtvQkFDWixnQkFBZ0I7b0JBQ2hCLFdBQVc7aUJBQ1o7Z0JBQ0QsZUFBZTtnQkFDZiwyQ0FBMkM7Z0JBQzNDLEtBQUs7Z0JBQ0wsT0FBTyxFQUFFO29CQUNQLHVCQUF1QjtvQkFDdkIsWUFBWTtpQkFDYjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMsIExPQ0FMRV9JRCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXG5pbXBvcnQgeyBCcm93c2VyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3NlcidcbmltcG9ydCB7IEJyb3dzZXJBbmltYXRpb25zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci9hbmltYXRpb25zJ1xuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSAgZnJvbSAnLi9tYXRlcmlhbC5tb2R1bGUnXG5pbXBvcnQgeyBTZXR0aW5nc01vZHVsZSB9IGZyb20gJy4vc2V0dGluZ3Mvc2V0dGluZ3MubW9kdWxlJ1xuaW1wb3J0IHsgSWZvZkNvbmZpZywgQ09SRV9DT05GSUcsIEFkbWluTW9kdWxlIH0gZnJvbSAnQGZvZi1hbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBMYXlvdXRNb2R1bGUgfSBmcm9tICcuL2xheW91dC9sYXlvdXQubW9kdWxlJ1xuLy8gaW1wb3J0IGxvY2FsZUZyIGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9sb2NhbGVzL2ZyJ1xuLy8gaW1wb3J0IHsgcmVnaXN0ZXJMb2NhbGVEYXRhIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJ1xuXG4vLyByZWdpc3RlckxvY2FsZURhdGEobG9jYWxlRnIsICdmcicpXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQnJvd3Nlck1vZHVsZSxcbiAgICBNYXRlcmlhbE1vZHVsZSwgICAgXG4gICAgU2V0dGluZ3NNb2R1bGUsXG4gICAgTGF5b3V0TW9kdWxlLFxuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgQWRtaW5Nb2R1bGVcbiAgXSxcbiAgLy8gcHJvdmlkZXJzOiBbXG4gIC8vICAge3Byb3ZpZGU6IExPQ0FMRV9JRCwgdXNlVmFsdWU6ICdmcicgfSxcbiAgLy8gXSxcbiAgZXhwb3J0czogW1xuICAgIEJyb3dzZXJBbmltYXRpb25zTW9kdWxlLCAgICBcbiAgICBMYXlvdXRNb2R1bGUgICAgXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgVGVtcGxhdGVNb2R1bGUgeyAgIFxuICBzdGF0aWMgZm9yUm9vdChmb2ZDb25maWc6IElmb2ZDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzPFRlbXBsYXRlTW9kdWxlPiB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBUZW1wbGF0ZU1vZHVsZSwgICAgICBcbiAgICAgIHByb3ZpZGVyczogWyAgICAgICAgXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBDT1JFX0NPTkZJRyxcbiAgICAgICAgICB1c2VWYWx1ZTogZm9mQ29uZmlnXG4gICAgICAgIH1cbiAgICAgIF1cbiAgICB9XG4gIH1cbn1cbiJdfQ==