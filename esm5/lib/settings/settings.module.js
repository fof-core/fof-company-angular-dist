import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsConfigComponent } from './settings-config/settings-config.component';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';
import * as i0 from "@angular/core";
var SettingsModule = /** @class */ (function () {
    function SettingsModule() {
    }
    SettingsModule.ɵmod = i0.ɵɵdefineNgModule({ type: SettingsModule });
    SettingsModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SettingsModule_Factory(t) { return new (t || SettingsModule)(); }, imports: [[
                CommonModule,
                SettingsRoutingModule,
                FormsModule,
                MaterialModule
            ]] });
    return SettingsModule;
}());
export { SettingsModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SettingsModule, { declarations: [SettingsConfigComponent], imports: [CommonModule,
        SettingsRoutingModule,
        FormsModule,
        MaterialModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    SettingsConfigComponent
                ],
                imports: [
                    CommonModule,
                    SettingsRoutingModule,
                    FormsModule,
                    MaterialModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9zZXR0aW5ncy9zZXR0aW5ncy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUN4QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFFOUMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUE7QUFDakUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkNBQTZDLENBQUE7QUFFckYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFBO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7QUFFNUM7SUFBQTtLQVcrQjtzREFBbEIsY0FBYzsrR0FBZCxjQUFjLGtCQVBoQjtnQkFDUCxZQUFZO2dCQUNaLHFCQUFxQjtnQkFDckIsV0FBVztnQkFDWCxjQUFjO2FBQ2Y7eUJBbEJIO0NBb0IrQixBQVgvQixJQVcrQjtTQUFsQixjQUFjO3dGQUFkLGNBQWMsbUJBVHZCLHVCQUF1QixhQUd2QixZQUFZO1FBQ1oscUJBQXFCO1FBQ3JCLFdBQVc7UUFDWCxjQUFjO2tEQUdMLGNBQWM7Y0FYMUIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWix1QkFBdUI7aUJBQ3hCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLHFCQUFxQjtvQkFDckIsV0FBVztvQkFDWCxjQUFjO2lCQUNmO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXG5cbmltcG9ydCB7IFNldHRpbmdzUm91dGluZ01vZHVsZSB9IGZyb20gJy4vc2V0dGluZ3Mtcm91dGluZy5tb2R1bGUnXG5pbXBvcnQgeyBTZXR0aW5nc0NvbmZpZ0NvbXBvbmVudCB9IGZyb20gJy4vc2V0dGluZ3MtY29uZmlnL3NldHRpbmdzLWNvbmZpZy5jb21wb25lbnQnXG5cbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJ1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3JtcydcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgU2V0dGluZ3NDb25maWdDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBTZXR0aW5nc1JvdXRpbmdNb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgTWF0ZXJpYWxNb2R1bGVcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBTZXR0aW5nc01vZHVsZSB7IH1cbiJdfQ==