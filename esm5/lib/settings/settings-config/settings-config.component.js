import { Component } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "../settings.service";
import * as i2 from "@angular/material/icon";
import * as i3 from "@angular/material/form-field";
import * as i4 from "@angular/material/slide-toggle";
import * as i5 from "@angular/material/select";
import * as i6 from "@angular/forms";
import * as i7 from "@angular/common";
import * as i8 from "@angular/material/core";
function SettingsConfigComponent_mat_option_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 9);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var t_r17 = ctx.$implicit;
    i0.ɵɵproperty("value", t_r17.value);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", t_r17.label, " ");
} }
var SettingsConfigComponent = /** @class */ (function () {
    function SettingsConfigComponent(settingsService) {
        var _this = this;
        this.settingsService = settingsService;
        // All private variables
        this.priVar = {
            themeSub: undefined,
            stickyHeaderSub: undefined,
            pageTransitionSub: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            themes: [
                { value: 'default-theme', label: 'Bleu' },
                { value: 'light-theme', label: 'Clair' },
                { value: 'nature-theme', label: 'Nature' },
                { value: 'black-theme', label: 'Nuit' }
            ],
            currentTheme: undefined,
            currentStickyHeader: undefined,
            currentPageTransition: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            onThemeSelect: function (value) {
                _this.settingsService.theme = value;
            },
            onStickyHeaderToggle: function (value) {
                _this.settingsService.stickyHeader = value;
            },
            onPageTransitionToggle: function (value) {
                _this.settingsService.pageTransition = value;
            }
        };
    }
    // Angular events
    SettingsConfigComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.themeSub = this.settingsService.themeChange
            .subscribe(function (theme) { _this.uiVar.currentTheme = theme; });
        this.priVar.stickyHeaderSub = this.settingsService.stickyHeaderChange
            .subscribe(function (value) { _this.uiVar.currentStickyHeader = value; });
        this.priVar.pageTransitionSub = this.settingsService.pageTransitionChange
            .subscribe(function (value) { _this.uiVar.currentPageTransition = value; });
    };
    SettingsConfigComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.themeSub) {
            this.priVar.themeSub.unsubscribe();
        }
        if (this.priVar.stickyHeaderSub) {
            this.priVar.stickyHeaderSub.unsubscribe();
        }
        if (this.priVar.pageTransitionSub) {
            this.priVar.pageTransitionSub.unsubscribe();
        }
    };
    SettingsConfigComponent.ɵfac = function SettingsConfigComponent_Factory(t) { return new (t || SettingsConfigComponent)(i0.ɵɵdirectiveInject(i1.SettingsService)); };
    SettingsConfigComponent.ɵcmp = i0.ɵɵdefineComponent({ type: SettingsConfigComponent, selectors: [["company-settings-config"]], decls: 32, vars: 5, consts: [[1, "container"], [1, "row"], [1, "col-sm-12"], [1, "col-md-6", "group"], [1, "icon-form-field"], ["color", "accent"], [3, "checked", "change"], ["name", "themes", 3, "placeholder", "ngModel", "selectionChange"], [3, "value", 4, "ngFor", "ngForOf"], [3, "value"]], template: function SettingsConfigComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "div", 1);
            i0.ɵɵelementStart(2, "div", 2);
            i0.ɵɵelementStart(3, "h1");
            i0.ɵɵtext(4, "Param\u00E8tres");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "div", 1);
            i0.ɵɵelementStart(6, "div", 3);
            i0.ɵɵelementStart(7, "h2");
            i0.ɵɵtext(8, "G\u00E9n\u00E9raux");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "div", 4);
            i0.ɵɵelementStart(10, "mat-icon", 5);
            i0.ɵɵtext(11, "menu");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(12, "mat-placeholder");
            i0.ɵɵtext(13, "Ent\u00EAte fixe");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(14, "mat-slide-toggle", 6);
            i0.ɵɵlistener("change", function SettingsConfigComponent_Template_mat_slide_toggle_change_14_listener($event) { return ctx.uiAction.onStickyHeaderToggle($event.checked); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(15, "h2");
            i0.ɵɵtext(16, "Th\u00E8me");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(17, "div", 4);
            i0.ɵɵelementStart(18, "mat-icon", 5);
            i0.ɵɵtext(19, "brush");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(20, "mat-form-field");
            i0.ɵɵelementStart(21, "mat-select", 7);
            i0.ɵɵlistener("selectionChange", function SettingsConfigComponent_Template_mat_select_selectionChange_21_listener($event) { return ctx.uiAction.onThemeSelect($event.value); });
            i0.ɵɵtemplate(22, SettingsConfigComponent_mat_option_22_Template, 2, 2, "mat-option", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(23, "div", 3);
            i0.ɵɵelementStart(24, "h2");
            i0.ɵɵtext(25, "Animations");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(26, "div", 4);
            i0.ɵɵelementStart(27, "mat-icon", 5);
            i0.ɵɵtext(28, "web_asset");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(29, "mat-placeholder");
            i0.ɵɵtext(30, "Transition douce lors de la navigation");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(31, "mat-slide-toggle", 6);
            i0.ɵɵlistener("change", function SettingsConfigComponent_Template_mat_slide_toggle_change_31_listener($event) { return ctx.uiAction.onPageTransitionToggle($event.checked); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(14);
            i0.ɵɵproperty("checked", ctx.uiVar.currentStickyHeader);
            i0.ɵɵadvance(7);
            i0.ɵɵproperty("placeholder", "Th\u00E8me de couleur")("ngModel", ctx.uiVar.currentTheme);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx.uiVar.themes);
            i0.ɵɵadvance(9);
            i0.ɵɵproperty("checked", ctx.uiVar.currentPageTransition);
        } }, directives: [i2.MatIcon, i3.MatPlaceholder, i4.MatSlideToggle, i3.MatFormField, i5.MatSelect, i6.NgControlStatus, i6.NgModel, i7.NgForOf, i8.MatOption], styles: [".container[_ngcontent-%COMP%]{margin-top:20px}h1[_ngcontent-%COMP%]{margin:0 0 20px;text-transform:uppercase}h2[_ngcontent-%COMP%]{margin:0 0 10px;text-transform:uppercase}.group[_ngcontent-%COMP%]{margin:0 0 40px}.icon-form-field[_ngcontent-%COMP%]{position:relative;display:flex;height:65.5px;align-items:center}.icon-form-field[_ngcontent-%COMP%]   mat-placeholder[_ngcontent-%COMP%]{flex:2 1 auto}mat-icon[_ngcontent-%COMP%]{margin:0 6px 6px 0;font-size:20px}mat-form-field[_ngcontent-%COMP%]{flex:1 0 auto}"] });
    return SettingsConfigComponent;
}());
export { SettingsConfigComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsConfigComponent, [{
        type: Component,
        args: [{
                selector: 'company-settings-config',
                templateUrl: './settings-config.component.html',
                styleUrls: ['./settings-config.component.scss']
            }]
    }], function () { return [{ type: i1.SettingsService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MtY29uZmlnLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvc2V0dGluZ3Mvc2V0dGluZ3MtY29uZmlnL3NldHRpbmdzLWNvbmZpZy5jb21wb25lbnQudHMiLCJsaWIvc2V0dGluZ3Mvc2V0dGluZ3MtY29uZmlnL3NldHRpbmdzLWNvbmZpZy5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixNQUFNLGVBQWUsQ0FBQTs7Ozs7Ozs7Ozs7SUN1QmhELHFDQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFhOzs7SUFGOEIsbUNBQWlCO0lBQzFELGVBQ0Y7SUFERSw0Q0FDRjs7QURyQlo7SUFNRSxpQ0FDbUIsZUFBZ0M7UUFEbkQsaUJBRUs7UUFEYyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFHbkQsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLFFBQVEsRUFBZ0IsU0FBUztZQUNqQyxlQUFlLEVBQWdCLFNBQVM7WUFDeEMsaUJBQWlCLEVBQWdCLFNBQVM7U0FDM0MsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUcsRUFDbEIsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixNQUFNLEVBQUU7Z0JBQ04sRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUU7Z0JBQ3pDLEVBQUUsS0FBSyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFO2dCQUN4QyxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRTtnQkFDMUMsRUFBRSxLQUFLLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUU7YUFDeEM7WUFDRCxZQUFZLEVBQVUsU0FBUztZQUMvQixtQkFBbUIsRUFBVyxTQUFTO1lBQ3ZDLHFCQUFxQixFQUFXLFNBQVM7U0FDMUMsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsYUFBYSxFQUFDLFVBQUMsS0FBYTtnQkFDMUIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO1lBQ3BDLENBQUM7WUFDRCxvQkFBb0IsRUFBQyxVQUFDLEtBQWM7Z0JBQ2xDLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQTtZQUMzQyxDQUFDO1lBQ0Qsc0JBQXNCLEVBQUMsVUFBQyxLQUFjO2dCQUNwQyxLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUE7WUFDN0MsQ0FBQztTQUNGLENBQUE7SUFsQ0csQ0FBQztJQW1DTCxpQkFBaUI7SUFDakIsMENBQVEsR0FBUjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXO2FBQ3BELFNBQVMsQ0FBQyxVQUFDLEtBQWEsSUFBTyxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNwRSxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQjthQUNsRSxTQUFTLENBQUMsVUFBQyxLQUFjLElBQU8sS0FBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUM1RSxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsb0JBQW9CO2FBQ3RFLFNBQVMsQ0FBQyxVQUFDLEtBQWMsSUFBTyxLQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFBO0lBQy9FLENBQUM7SUFDRCw2Q0FBVyxHQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQUU7UUFDaEUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRTtZQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQUU7UUFDOUUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFO1lBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUFFO0lBQ3BGLENBQUM7a0dBbkRVLHVCQUF1QjtnRUFBdkIsdUJBQXVCO1lDVHBDLDhCQUNFO1lBQUEsOEJBQ0U7WUFBQSw4QkFBdUI7WUFBQSwwQkFBSTtZQUFBLCtCQUFVO1lBQUEsaUJBQUs7WUFBQSxpQkFBTTtZQUNsRCxpQkFBTTtZQUNOLDhCQUNFO1lBQUEsOEJBQ0U7WUFBQSwwQkFBSTtZQUFBLGtDQUFRO1lBQUEsaUJBQUs7WUFDakIsOEJBQ0U7WUFBQSxvQ0FBeUI7WUFBQSxxQkFBSTtZQUFBLGlCQUFXO1lBQ3hDLHdDQUFpQjtZQUFBLGlDQUFXO1lBQUEsaUJBQWtCO1lBQzlDLDRDQUdtQjtZQURqQix1SEFBVSxpREFBNkMsSUFBQztZQUMxRCxpQkFBbUI7WUFDckIsaUJBQU07WUFDTiwyQkFBSTtZQUFBLDJCQUFLO1lBQUEsaUJBQUs7WUFDZCwrQkFDRTtZQUFBLG9DQUF5QjtZQUFBLHNCQUFLO1lBQUEsaUJBQVc7WUFDekMsdUNBQ0U7WUFBQSxzQ0FJRTtZQUZFLG1JQUFtQix3Q0FBb0MsSUFBQztZQUUxRCx3RkFDRTtZQUVKLGlCQUFhO1lBQ2YsaUJBQWlCO1lBQ25CLGlCQUFNO1lBQ1IsaUJBQU07WUFDTiwrQkFDRTtZQUFBLDJCQUFJO1lBQUEsMkJBQVU7WUFBQSxpQkFBSztZQUNuQiwrQkFDRTtZQUFBLG9DQUF5QjtZQUFBLDBCQUFTO1lBQUEsaUJBQVc7WUFDN0Msd0NBQWlCO1lBQUEsdURBQXNDO1lBQUEsaUJBQWtCO1lBQ3pFLDRDQUdtQjtZQURqQix1SEFBVSxtREFBK0MsSUFBQztZQUM1RCxpQkFBbUI7WUFDckIsaUJBQU07WUFDUixpQkFBTTtZQUNSLGlCQUFNO1lBQ1IsaUJBQU07O1lBL0JJLGdCQUFxQztZQUFyQyx1REFBcUM7WUFRekIsZUFBa0M7WUFBbEMscURBQWtDLG1DQUFBO1lBSWhDLGVBQThCO1lBQTlCLDBDQUE4QjtZQWE1QyxlQUF1QztZQUF2Qyx5REFBdUM7O2tDRHBDakQ7Q0E2REMsQUF6REQsSUF5REM7U0FwRFksdUJBQXVCO2tEQUF2Qix1QkFBdUI7Y0FMbkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSx5QkFBeUI7Z0JBQ25DLFdBQVcsRUFBRSxrQ0FBa0M7Z0JBQy9DLFNBQVMsRUFBRSxDQUFDLGtDQUFrQyxDQUFDO2FBQ2hEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBTZXR0aW5nc1NlcnZpY2UgfSBmcm9tICcuLi9zZXR0aW5ncy5zZXJ2aWNlJ1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcydcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnY29tcGFueS1zZXR0aW5ncy1jb25maWcnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2V0dGluZ3MtY29uZmlnLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2V0dGluZ3MtY29uZmlnLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgU2V0dGluZ3NDb25maWdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJlYWRvbmx5IHNldHRpbmdzU2VydmljZTogU2V0dGluZ3NTZXJ2aWNlXG4gICkgeyB9XG5cbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXG4gIHByaXZhdGUgcHJpVmFyID0ge1xuICAgIHRoZW1lU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZCxcbiAgICBzdGlja3lIZWFkZXJTdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkLFxuICAgIHBhZ2VUcmFuc2l0aW9uU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZFxuICB9XG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xuICBwcml2YXRlIHByaXZGdW5jID0ge1xuICB9XG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXG4gIHB1YmxpYyB1aVZhciA9IHtcbiAgICB0aGVtZXM6IFtcbiAgICAgIHsgdmFsdWU6ICdkZWZhdWx0LXRoZW1lJywgbGFiZWw6ICdCbGV1JyB9LFxuICAgICAgeyB2YWx1ZTogJ2xpZ2h0LXRoZW1lJywgbGFiZWw6ICdDbGFpcicgfSxcbiAgICAgIHsgdmFsdWU6ICduYXR1cmUtdGhlbWUnLCBsYWJlbDogJ05hdHVyZScgfSxcbiAgICAgIHsgdmFsdWU6ICdibGFjay10aGVtZScsIGxhYmVsOiAnTnVpdCcgfVxuICAgIF0sXG4gICAgY3VycmVudFRoZW1lOiA8c3RyaW5nPnVuZGVmaW5lZCxcbiAgICBjdXJyZW50U3RpY2t5SGVhZGVyOiA8Ym9vbGVhbj51bmRlZmluZWQsXG4gICAgY3VycmVudFBhZ2VUcmFuc2l0aW9uOiA8Ym9vbGVhbj51bmRlZmluZWRcbiAgfVxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcbiAgcHVibGljIHVpQWN0aW9uID0ge1xuICAgIG9uVGhlbWVTZWxlY3Q6KHZhbHVlOiBzdHJpbmcpID0+IHtcbiAgICAgIHRoaXMuc2V0dGluZ3NTZXJ2aWNlLnRoZW1lID0gdmFsdWVcbiAgICB9LFxuICAgIG9uU3RpY2t5SGVhZGVyVG9nZ2xlOih2YWx1ZTogYm9vbGVhbikgPT4geyAgICAgICAgXG4gICAgICB0aGlzLnNldHRpbmdzU2VydmljZS5zdGlja3lIZWFkZXIgPSB2YWx1ZVxuICAgIH0sXG4gICAgb25QYWdlVHJhbnNpdGlvblRvZ2dsZToodmFsdWU6IGJvb2xlYW4pID0+IHsgICAgICAgIFxuICAgICAgdGhpcy5zZXR0aW5nc1NlcnZpY2UucGFnZVRyYW5zaXRpb24gPSB2YWx1ZVxuICAgIH1cbiAgfVxuICAvLyBBbmd1bGFyIGV2ZW50c1xuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnByaVZhci50aGVtZVN1YiA9IHRoaXMuc2V0dGluZ3NTZXJ2aWNlLnRoZW1lQ2hhbmdlXG4gICAgICAuc3Vic2NyaWJlKCh0aGVtZTogc3RyaW5nKSA9PiB7IHRoaXMudWlWYXIuY3VycmVudFRoZW1lID0gdGhlbWUgfSlcbiAgICB0aGlzLnByaVZhci5zdGlja3lIZWFkZXJTdWIgPSB0aGlzLnNldHRpbmdzU2VydmljZS5zdGlja3lIZWFkZXJDaGFuZ2VcbiAgICAgIC5zdWJzY3JpYmUoKHZhbHVlOiBib29sZWFuKSA9PiB7IHRoaXMudWlWYXIuY3VycmVudFN0aWNreUhlYWRlciA9IHZhbHVlIH0pXG4gICAgdGhpcy5wcmlWYXIucGFnZVRyYW5zaXRpb25TdWIgPSB0aGlzLnNldHRpbmdzU2VydmljZS5wYWdlVHJhbnNpdGlvbkNoYW5nZVxuICAgICAgLnN1YnNjcmliZSgodmFsdWU6IGJvb2xlYW4pID0+IHsgdGhpcy51aVZhci5jdXJyZW50UGFnZVRyYW5zaXRpb24gPSB2YWx1ZX0pXG4gIH0gIFxuICBuZ09uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5wcmlWYXIudGhlbWVTdWIpIHsgdGhpcy5wcmlWYXIudGhlbWVTdWIudW5zdWJzY3JpYmUoKSB9XG4gICAgaWYgKHRoaXMucHJpVmFyLnN0aWNreUhlYWRlclN1YikgeyB0aGlzLnByaVZhci5zdGlja3lIZWFkZXJTdWIudW5zdWJzY3JpYmUoKSB9XG4gICAgaWYgKHRoaXMucHJpVmFyLnBhZ2VUcmFuc2l0aW9uU3ViKSB7IHRoaXMucHJpVmFyLnBhZ2VUcmFuc2l0aW9uU3ViLnVuc3Vic2NyaWJlKCkgfVxuICB9XG59XG4iLCI8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XG4gIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTEyXCI+PGgxPlBhcmFtw6h0cmVzPC9oMT48L2Rpdj5cbiAgPC9kaXY+XG4gIDxkaXYgY2xhc3M9XCJyb3dcIj4gICAgXG4gICAgPGRpdiBjbGFzcz1cImNvbC1tZC02IGdyb3VwXCI+XG4gICAgICA8aDI+R8OpbsOpcmF1eDwvaDI+XG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvbi1mb3JtLWZpZWxkXCI+ICAgICAgICBcbiAgICAgICAgPG1hdC1pY29uIGNvbG9yPVwiYWNjZW50XCI+bWVudTwvbWF0LWljb24+XG4gICAgICAgIDxtYXQtcGxhY2Vob2xkZXI+RW50w6p0ZSBmaXhlPC9tYXQtcGxhY2Vob2xkZXI+XG4gICAgICAgIDxtYXQtc2xpZGUtdG9nZ2xlXG4gICAgICAgICAgW2NoZWNrZWRdPVwidWlWYXIuY3VycmVudFN0aWNreUhlYWRlclwiXG4gICAgICAgICAgKGNoYW5nZSk9XCJ1aUFjdGlvbi5vblN0aWNreUhlYWRlclRvZ2dsZSgkZXZlbnQuY2hlY2tlZClcIj5cbiAgICAgICAgPC9tYXQtc2xpZGUtdG9nZ2xlPlxuICAgICAgPC9kaXY+XG4gICAgICA8aDI+VGjDqG1lPC9oMj5cbiAgICAgIDxkaXYgY2xhc3M9XCJpY29uLWZvcm0tZmllbGRcIj5cbiAgICAgICAgPG1hdC1pY29uIGNvbG9yPVwiYWNjZW50XCI+YnJ1c2g8L21hdC1pY29uPlxuICAgICAgICA8bWF0LWZvcm0tZmllbGQ+ICAgICAgICAgIFxuICAgICAgICAgIDxtYXQtc2VsZWN0IFtwbGFjZWhvbGRlcl09XCInVGjDqG1lIGRlIGNvdWxldXInXCIgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgIFtuZ01vZGVsXT1cInVpVmFyLmN1cnJlbnRUaGVtZVwiXG4gICAgICAgICAgICAgIChzZWxlY3Rpb25DaGFuZ2UpPVwidWlBY3Rpb24ub25UaGVtZVNlbGVjdCgkZXZlbnQudmFsdWUpXCJcbiAgICAgICAgICAgICAgbmFtZT1cInRoZW1lc1wiPlxuICAgICAgICAgICAgPG1hdC1vcHRpb24gKm5nRm9yPVwibGV0IHQgb2YgdWlWYXIudGhlbWVzXCIgW3ZhbHVlXT1cInQudmFsdWVcIj5cbiAgICAgICAgICAgICAge3sgdC5sYWJlbCB9fVxuICAgICAgICAgICAgPC9tYXQtb3B0aW9uPlxuICAgICAgICAgIDwvbWF0LXNlbGVjdD5cbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cbiAgICAgIDwvZGl2PiAgICAgIFxuICAgIDwvZGl2PlxuICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNiBncm91cFwiPlxuICAgICAgPGgyPkFuaW1hdGlvbnM8L2gyPlxuICAgICAgPGRpdiBjbGFzcz1cImljb24tZm9ybS1maWVsZFwiPiAgICAgICAgXG4gICAgICAgIDxtYXQtaWNvbiBjb2xvcj1cImFjY2VudFwiPndlYl9hc3NldDwvbWF0LWljb24+XG4gICAgICAgIDxtYXQtcGxhY2Vob2xkZXI+VHJhbnNpdGlvbiBkb3VjZSBsb3JzIGRlIGxhIG5hdmlnYXRpb248L21hdC1wbGFjZWhvbGRlcj5cbiAgICAgICAgPG1hdC1zbGlkZS10b2dnbGVcbiAgICAgICAgICBbY2hlY2tlZF09XCJ1aVZhci5jdXJyZW50UGFnZVRyYW5zaXRpb25cIlxuICAgICAgICAgIChjaGFuZ2UpPVwidWlBY3Rpb24ub25QYWdlVHJhbnNpdGlvblRvZ2dsZSgkZXZlbnQuY2hlY2tlZClcIj5cbiAgICAgICAgPC9tYXQtc2xpZGUtdG9nZ2xlPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuPC9kaXY+XG5cblxuIl19