import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SettingsConfigComponent } from './settings-config/settings-config.component';
import { FofAuthGuard } from '@fof-angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
var routes = [
    { path: 'settings', component: SettingsConfigComponent,
        data: { title: 'Configuration' }, canActivate: [FofAuthGuard] }
];
var SettingsRoutingModule = /** @class */ (function () {
    function SettingsRoutingModule() {
    }
    SettingsRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: SettingsRoutingModule });
    SettingsRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SettingsRoutingModule_Factory(t) { return new (t || SettingsRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
            RouterModule] });
    return SettingsRoutingModule;
}());
export { SettingsRoutingModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SettingsRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3Mtcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY29tcGFueS1hbmd1bGFyL3RlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL3NldHRpbmdzL3NldHRpbmdzLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFVLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQ3RELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFBO0FBQ3JGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQTs7O0FBRWhELElBQU0sTUFBTSxHQUFXO0lBQ3JCLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsdUJBQXVCO1FBQ3BELElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsRUFBRSxXQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUMsRUFBQztDQUNqRSxDQUFBO0FBRUQ7SUFBQTtLQUlzQzs2REFBekIscUJBQXFCOzZIQUFyQixxQkFBcUIsa0JBSHZCLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM5QixZQUFZO2dDQVp4QjtDQWNzQyxBQUp0QyxJQUlzQztTQUF6QixxQkFBcUI7d0ZBQXJCLHFCQUFxQiwwQ0FGdEIsWUFBWTtrREFFWCxxQkFBcUI7Y0FKakMsUUFBUTtlQUFDO2dCQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQzthQUN4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcbmltcG9ydCB7IFJvdXRlcywgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xuaW1wb3J0IHsgU2V0dGluZ3NDb25maWdDb21wb25lbnQgfSBmcm9tICcuL3NldHRpbmdzLWNvbmZpZy9zZXR0aW5ncy1jb25maWcuY29tcG9uZW50J1xuaW1wb3J0IHsgRm9mQXV0aEd1YXJkIH0gZnJvbSAnQGZvZi1hbmd1bGFyL2NvcmUnXG5cbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuICB7IHBhdGg6ICdzZXR0aW5ncycsIGNvbXBvbmVudDogU2V0dGluZ3NDb25maWdDb21wb25lbnQsIFxuICAgIGRhdGE6IHsgdGl0bGU6ICdDb25maWd1cmF0aW9uJyB9LCBjYW5BY3RpdmF0ZTogW0ZvZkF1dGhHdWFyZF19XG5dXG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXG4gIGV4cG9ydHM6IFtSb3V0ZXJNb2R1bGVdXG59KVxuZXhwb3J0IGNsYXNzIFNldHRpbmdzUm91dGluZ01vZHVsZSB7IH1cbiJdfQ==