import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@fof-angular/core";
var SettingsService = /** @class */ (function () {
    function SettingsService(fofLocalstorageService) {
        this.fofLocalstorageService = fofLocalstorageService;
        this._theme = undefined;
        this._themeChange = new BehaviorSubject(undefined);
        this.themeChange = this._themeChange.asObservable();
        this._stickyHeader = undefined;
        this._stickyHeaderChange = new BehaviorSubject(undefined);
        this.stickyHeaderChange = this._stickyHeaderChange.asObservable();
        this._pageTransition = undefined;
        this._pageTransitionChange = new BehaviorSubject(undefined);
        var theme = this.fofLocalstorageService.getItem('theme');
        if (theme) {
            this.theme = theme;
        }
        else {
            this.theme = 'light-theme';
        }
        var stickyHeader = this.fofLocalstorageService.getItem('stickyHeader');
        this.stickyHeader = stickyHeader;
        var pageTransition = this.fofLocalstorageService.getItem('pageTransition');
        this.pageTransition = pageTransition;
    }
    Object.defineProperty(SettingsService.prototype, "theme", {
        get: function () {
            return this._theme;
        },
        set: function (theme) {
            this._theme = theme;
            this._themeChange.next(this._theme);
            this.fofLocalstorageService.setItem('theme', this._theme);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SettingsService.prototype, "stickyHeader", {
        get: function () {
            return this._stickyHeader;
        },
        set: function (stickyHeader) {
            this._stickyHeader = stickyHeader;
            this._stickyHeaderChange.next(this._stickyHeader);
            this.fofLocalstorageService.setItem('stickyHeader', this._stickyHeader);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SettingsService.prototype, "pageTransitionChange", {
        get: function () {
            return this._pageTransitionChange.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SettingsService.prototype, "pageTransition", {
        get: function () {
            return this._pageTransition;
        },
        set: function (pageTransition) {
            this._pageTransition = pageTransition;
            this._pageTransitionChange.next(this._pageTransition);
            this.fofLocalstorageService.setItem('pageTransition', this._pageTransition);
        },
        enumerable: true,
        configurable: true
    });
    SettingsService.ɵfac = function SettingsService_Factory(t) { return new (t || SettingsService)(i0.ɵɵinject(i1.FofLocalstorageService)); };
    SettingsService.ɵprov = i0.ɵɵdefineInjectable({ token: SettingsService, factory: SettingsService.ɵfac, providedIn: 'root' });
    return SettingsService;
}());
export { SettingsService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SettingsService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.FofLocalstorageService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3Muc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvc2V0dGluZ3Mvc2V0dGluZ3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQzFDLE9BQU8sRUFBRSxlQUFlLEVBQWMsTUFBTSxNQUFNLENBQUE7OztBQUlsRDtJQUtFLHlCQUNVLHNCQUE4QztRQUE5QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBZWhELFdBQU0sR0FBVyxTQUFTLENBQUE7UUFDMUIsaUJBQVksR0FBNEIsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDOUQsZ0JBQVcsR0FBdUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQVcxRSxrQkFBYSxHQUFZLFNBQVMsQ0FBQTtRQUNsQyx3QkFBbUIsR0FBNkIsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDdEUsdUJBQWtCLEdBQXdCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQVd6RixvQkFBZSxHQUFZLFNBQVMsQ0FBQTtRQUNwQywwQkFBcUIsR0FBNkIsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUE7UUF4Q3RGLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDeEQsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQTtTQUNuQjthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUE7U0FDM0I7UUFDRCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3RFLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFBO1FBRWhDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUMxRSxJQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQTtJQUN0QyxDQUFDO0lBTUQsc0JBQVcsa0NBQUs7YUFBaEI7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUE7UUFDcEIsQ0FBQzthQUNELFVBQWlCLEtBQUs7WUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUE7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ25DLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUMzRCxDQUFDOzs7T0FMQTtJQVdELHNCQUFXLHlDQUFZO2FBQXZCO1lBQ0UsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFBO1FBQzNCLENBQUM7YUFDRCxVQUF3QixZQUFZO1lBQ2xDLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFBO1lBQ2pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBQ2pELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUN6RSxDQUFDOzs7T0FMQTtJQVNELHNCQUFXLGlEQUFvQjthQUEvQjtZQUNFLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxDQUFBO1FBQ2xELENBQUM7OztPQUFBO0lBRUQsc0JBQVcsMkNBQWM7YUFBekI7WUFDRSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUE7UUFDN0IsQ0FBQzthQUNELFVBQTBCLGNBQWM7WUFDdEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUE7WUFDckMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7WUFDckQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDN0UsQ0FBQzs7O09BTEE7a0ZBcERVLGVBQWU7MkRBQWYsZUFBZSxXQUFmLGVBQWUsbUJBRmQsTUFBTTswQkFOcEI7Q0FrRUMsQUE3REQsSUE2REM7U0ExRFksZUFBZTtrREFBZixlQUFlO2NBSDNCLFVBQVU7ZUFBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcydcbmltcG9ydCB7IEZvZkxvY2Fsc3RvcmFnZVNlcnZpY2UgfSBmcm9tICdAZm9mLWFuZ3VsYXIvY29yZSdcblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTZXR0aW5nc1NlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZm9mTG9jYWxzdG9yYWdlU2VydmljZTogRm9mTG9jYWxzdG9yYWdlU2VydmljZVxuICApIHsgXG4gICAgbGV0IHRoZW1lID0gdGhpcy5mb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlLmdldEl0ZW0oJ3RoZW1lJylcbiAgICBpZiAodGhlbWUpIHtcbiAgICAgIHRoaXMudGhlbWUgPSB0aGVtZVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnRoZW1lID0gJ2xpZ2h0LXRoZW1lJ1xuICAgIH1cbiAgICBsZXQgc3RpY2t5SGVhZGVyID0gdGhpcy5mb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlLmdldEl0ZW0oJ3N0aWNreUhlYWRlcicpXG4gICAgdGhpcy5zdGlja3lIZWFkZXIgPSBzdGlja3lIZWFkZXJcbiAgICBcbiAgICBsZXQgcGFnZVRyYW5zaXRpb24gPSB0aGlzLmZvZkxvY2Fsc3RvcmFnZVNlcnZpY2UuZ2V0SXRlbSgncGFnZVRyYW5zaXRpb24nKVxuICAgIHRoaXMucGFnZVRyYW5zaXRpb24gPSBwYWdlVHJhbnNpdGlvblxuICB9XG5cbiAgcHJpdmF0ZSBfdGhlbWU6IHN0cmluZyA9IHVuZGVmaW5lZFxuICBwcml2YXRlIF90aGVtZUNoYW5nZTogQmVoYXZpb3JTdWJqZWN0PHN0cmluZz4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0KHVuZGVmaW5lZCkgIFxuICBwdWJsaWMgcmVhZG9ubHkgdGhlbWVDaGFuZ2U6IE9ic2VydmFibGU8c3RyaW5nPiA9IHRoaXMuX3RoZW1lQ2hhbmdlLmFzT2JzZXJ2YWJsZSgpXG5cbiAgcHVibGljIGdldCB0aGVtZSgpIHtcbiAgICByZXR1cm4gdGhpcy5fdGhlbWVcbiAgfVxuICBwdWJsaWMgc2V0IHRoZW1lKHRoZW1lKSB7XG4gICAgdGhpcy5fdGhlbWUgPSB0aGVtZVxuICAgIHRoaXMuX3RoZW1lQ2hhbmdlLm5leHQodGhpcy5fdGhlbWUpXG4gICAgdGhpcy5mb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlLnNldEl0ZW0oJ3RoZW1lJywgdGhpcy5fdGhlbWUpXG4gIH1cblxuICBwcml2YXRlIF9zdGlja3lIZWFkZXI6IGJvb2xlYW4gPSB1bmRlZmluZWRcbiAgcHJpdmF0ZSBfc3RpY2t5SGVhZGVyQ2hhbmdlOiBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0KHVuZGVmaW5lZCkgIFxuICBwdWJsaWMgcmVhZG9ubHkgc3RpY2t5SGVhZGVyQ2hhbmdlOiBPYnNlcnZhYmxlPGJvb2xlYW4+ID0gdGhpcy5fc3RpY2t5SGVhZGVyQ2hhbmdlLmFzT2JzZXJ2YWJsZSgpXG5cbiAgcHVibGljIGdldCBzdGlja3lIZWFkZXIoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3N0aWNreUhlYWRlclxuICB9XG4gIHB1YmxpYyBzZXQgc3RpY2t5SGVhZGVyKHN0aWNreUhlYWRlcikge1xuICAgIHRoaXMuX3N0aWNreUhlYWRlciA9IHN0aWNreUhlYWRlclxuICAgIHRoaXMuX3N0aWNreUhlYWRlckNoYW5nZS5uZXh0KHRoaXMuX3N0aWNreUhlYWRlcilcbiAgICB0aGlzLmZvZkxvY2Fsc3RvcmFnZVNlcnZpY2Uuc2V0SXRlbSgnc3RpY2t5SGVhZGVyJywgdGhpcy5fc3RpY2t5SGVhZGVyKVxuICB9XG5cbiAgcHJpdmF0ZSBfcGFnZVRyYW5zaXRpb246IGJvb2xlYW4gPSB1bmRlZmluZWRcbiAgcHJpdmF0ZSBfcGFnZVRyYW5zaXRpb25DaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPiA9IG5ldyBCZWhhdmlvclN1YmplY3QodW5kZWZpbmVkKSAgXG4gIHB1YmxpYyBnZXQgcGFnZVRyYW5zaXRpb25DaGFuZ2UoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3BhZ2VUcmFuc2l0aW9uQ2hhbmdlLmFzT2JzZXJ2YWJsZSgpXG4gIH1cblxuICBwdWJsaWMgZ2V0IHBhZ2VUcmFuc2l0aW9uKCkge1xuICAgIHJldHVybiB0aGlzLl9wYWdlVHJhbnNpdGlvblxuICB9XG4gIHB1YmxpYyBzZXQgcGFnZVRyYW5zaXRpb24ocGFnZVRyYW5zaXRpb24pIHtcbiAgICB0aGlzLl9wYWdlVHJhbnNpdGlvbiA9IHBhZ2VUcmFuc2l0aW9uXG4gICAgdGhpcy5fcGFnZVRyYW5zaXRpb25DaGFuZ2UubmV4dCh0aGlzLl9wYWdlVHJhbnNpdGlvbilcbiAgICB0aGlzLmZvZkxvY2Fsc3RvcmFnZVNlcnZpY2Uuc2V0SXRlbSgncGFnZVRyYW5zaXRpb24nLCB0aGlzLl9wYWdlVHJhbnNpdGlvbilcbiAgfVxufVxuIl19