import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common'
// import { ScrollingModule } from '@angular/cdk/scrolling'
// import { MatAutocompleteModule } from '@angular/material/autocomplete'
// import { MatBadgeModule } from '@angular/material/badge'
// import { MatBottomSheetModule } from '@angular/material/bottom-sheet'
import { MatButtonModule } from '@angular/material/button';
// import { MatButtonToggleModule } from '@angular/material/button-toggle'
import { MatCardModule } from '@angular/material/card';
// import { MatCheckboxModule } from '@angular/material/checkbox'
// import { MatChipsModule } from '@angular/material/chips'
// import { MatNativeDateModule, MatRippleModule } from '@angular/material/core'
// import { MatDatepickerModule } from '@angular/material/datepicker'
// import { MatDialogModule } from '@angular/material/dialog'
import { MatDividerModule } from '@angular/material/divider';
// import { MatExpansionModule } from '@angular/material/expansion'
// import { MatGridListModule } from '@angular/material/grid-list'
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
// import { MatListModule } from '@angular/material/list'
import { MatMenuModule } from '@angular/material/menu';
// import { MatPaginatorModule } from '@angular/material/paginator'
// import { MatProgressBarModule } from '@angular/material/progress-bar'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatRadioModule } from '@angular/material/radio'
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSliderModule } from '@angular/material/slider'
// import { MatSnackBarModule } from '@angular/material/snack-bar'
// import { MatSortModule } from '@angular/material/sort'
// import { MatStepperModule } from '@angular/material/stepper'
// import { MatTableModule } from '@angular/material/table'
// import { MatTabsModule } from '@angular/material/tabs'
import { MatToolbarModule } from '@angular/material/toolbar';
import * as i0 from "@angular/core";
// import { MatTooltipModule } from '@angular/material/tooltip'
// import { MatTreeModule } from '@angular/material/tree'
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule.ɵmod = i0.ɵɵdefineNgModule({ type: MaterialModule });
    MaterialModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [[
                // MatAutocompleteModule,
                // MatBadgeModule,
                // MatBottomSheetModule,
                MatButtonModule,
                // MatButtonToggleModule,
                MatCardModule,
                // MatCheckboxModule,
                // MatChipsModule,
                // MatDatepickerModule,
                // MatDialogModule,
                MatDividerModule,
                // MatExpansionModule,
                // MatGridListModule,
                MatIconModule,
                MatInputModule,
                // MatListModule,
                MatMenuModule,
                // MatNativeDateModule,
                // MatPaginatorModule,
                // MatProgressBarModule,
                MatProgressSpinnerModule,
                // MatRadioModule,
                // MatRippleModule,
                MatSelectModule,
                MatSidenavModule,
                // MatSliderModule,
                MatSlideToggleModule,
                // MatSnackBarModule,
                // MatSortModule,
                // MatStepperModule,
                // MatTableModule,
                // MatTabsModule,
                MatToolbarModule,
            ],
            // MatAutocompleteModule,
            // MatBadgeModule,
            // MatBottomSheetModule,
            MatButtonModule,
            // MatButtonToggleModule,
            MatCardModule,
            // MatCheckboxModule,
            // MatChipsModule,
            // MatDatepickerModule,
            // MatDialogModule,
            MatDividerModule,
            // MatExpansionModule,
            // MatGridListModule,
            MatIconModule,
            MatInputModule,
            // MatListModule,
            MatMenuModule,
            // MatNativeDateModule,
            // MatPaginatorModule,
            // MatProgressBarModule,
            MatProgressSpinnerModule,
            // MatRadioModule,
            // MatRippleModule,
            MatSelectModule,
            MatSidenavModule,
            // MatSliderModule,
            MatSlideToggleModule,
            // MatSnackBarModule,
            // MatSortModule,
            // MatStepperModule,
            // MatTableModule,
            // MatTabsModule,
            MatToolbarModule] });
    return MaterialModule;
}());
export { MaterialModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MaterialModule, { imports: [
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        // MatCheckboxModule,
        // MatChipsModule,
        // MatDatepickerModule,
        // MatDialogModule,
        MatDividerModule,
        // MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        // MatListModule,
        MatMenuModule,
        // MatNativeDateModule,
        // MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        // MatSliderModule,
        MatSlideToggleModule,
        // MatSnackBarModule,
        // MatSortModule,
        // MatStepperModule,
        // MatTableModule,
        // MatTabsModule,
        MatToolbarModule], exports: [
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        // MatCheckboxModule,
        // MatChipsModule,
        // MatDatepickerModule,
        // MatDialogModule,
        MatDividerModule,
        // MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        // MatListModule,
        MatMenuModule,
        // MatNativeDateModule,
        // MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        // MatSliderModule,
        MatSlideToggleModule,
        // MatSnackBarModule,
        // MatSortModule,
        // MatStepperModule,
        // MatTableModule,
        // MatTabsModule,
        MatToolbarModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaterialModule, [{
        type: NgModule,
        args: [{
                imports: [
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    MatButtonModule,
                    // MatButtonToggleModule,
                    MatCardModule,
                    // MatCheckboxModule,
                    // MatChipsModule,
                    // MatDatepickerModule,
                    // MatDialogModule,
                    MatDividerModule,
                    // MatExpansionModule,
                    // MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    // MatListModule,
                    MatMenuModule,
                    // MatNativeDateModule,
                    // MatPaginatorModule,
                    // MatProgressBarModule,
                    MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    MatSelectModule,
                    MatSidenavModule,
                    // MatSliderModule,
                    MatSlideToggleModule,
                    // MatSnackBarModule,
                    // MatSortModule,
                    // MatStepperModule,
                    // MatTableModule,
                    // MatTabsModule,
                    MatToolbarModule,
                ],
                exports: [
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    MatButtonModule,
                    // MatButtonToggleModule,
                    MatCardModule,
                    // MatCheckboxModule,
                    // MatChipsModule,
                    // MatDatepickerModule,
                    // MatDialogModule,
                    MatDividerModule,
                    // MatExpansionModule,
                    // MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    // MatListModule,
                    MatMenuModule,
                    // MatNativeDateModule,
                    // MatPaginatorModule,
                    // MatProgressBarModule,
                    MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    MatSelectModule,
                    MatSidenavModule,
                    // MatSliderModule,
                    MatSlideToggleModule,
                    // MatSnackBarModule,
                    // MatSortModule,
                    // MatStepperModule,
                    // MatTableModule,
                    // MatTabsModule,
                    MatToolbarModule,
                ],
                declarations: []
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9tYXRlcmlhbC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUN4QyxpREFBaUQ7QUFDakQsMkRBQTJEO0FBQzNELHlFQUF5RTtBQUN6RSwyREFBMkQ7QUFDM0Qsd0VBQXdFO0FBQ3hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQTtBQUMxRCwwRUFBMEU7QUFDMUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQ3RELGlFQUFpRTtBQUNqRSwyREFBMkQ7QUFDM0QsZ0ZBQWdGO0FBQ2hGLHFFQUFxRTtBQUNyRSw2REFBNkQ7QUFDN0QsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUE7QUFDNUQsbUVBQW1FO0FBQ25FLGtFQUFrRTtBQUNsRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7QUFFdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFBO0FBQ3hELHlEQUF5RDtBQUN6RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7QUFDdEQsbUVBQW1FO0FBQ25FLHdFQUF3RTtBQUN4RSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQTtBQUM3RSwyREFBMkQ7QUFDM0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFBO0FBQzFELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFBO0FBQzVELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdDQUFnQyxDQUFBO0FBQ3JFLDZEQUE2RDtBQUM3RCxrRUFBa0U7QUFDbEUseURBQXlEO0FBQ3pELCtEQUErRDtBQUMvRCwyREFBMkQ7QUFDM0QseURBQXlEO0FBQ3pELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFBOztBQUM1RCwrREFBK0Q7QUFDL0QseURBQXlEO0FBRXpEO0lBQUE7S0E2RStCO3NEQUFsQixjQUFjOytHQUFkLGNBQWMsa0JBNUVoQjtnQkFDUCx5QkFBeUI7Z0JBQ3pCLGtCQUFrQjtnQkFDbEIsd0JBQXdCO2dCQUN4QixlQUFlO2dCQUNmLHlCQUF5QjtnQkFDekIsYUFBYTtnQkFDYixxQkFBcUI7Z0JBQ3JCLGtCQUFrQjtnQkFDbEIsdUJBQXVCO2dCQUN2QixtQkFBbUI7Z0JBQ25CLGdCQUFnQjtnQkFDaEIsc0JBQXNCO2dCQUN0QixxQkFBcUI7Z0JBQ3JCLGFBQWE7Z0JBQ2IsY0FBYztnQkFDZCxpQkFBaUI7Z0JBQ2pCLGFBQWE7Z0JBQ2IsdUJBQXVCO2dCQUN2QixzQkFBc0I7Z0JBQ3RCLHdCQUF3QjtnQkFDeEIsd0JBQXdCO2dCQUN4QixrQkFBa0I7Z0JBQ2xCLG1CQUFtQjtnQkFDbkIsZUFBZTtnQkFDZixnQkFBZ0I7Z0JBQ2hCLG1CQUFtQjtnQkFDbkIsb0JBQW9CO2dCQUNwQixxQkFBcUI7Z0JBQ3JCLGlCQUFpQjtnQkFDakIsb0JBQW9CO2dCQUNwQixrQkFBa0I7Z0JBQ2xCLGlCQUFpQjtnQkFDakIsZ0JBQWdCO2FBR2pCO1lBRUEseUJBQXlCO1lBQ3hCLGtCQUFrQjtZQUNsQix3QkFBd0I7WUFDeEIsZUFBZTtZQUNmLHlCQUF5QjtZQUN6QixhQUFhO1lBQ2IscUJBQXFCO1lBQ3JCLGtCQUFrQjtZQUNsQix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLGdCQUFnQjtZQUNoQixzQkFBc0I7WUFDdEIscUJBQXFCO1lBQ3JCLGFBQWE7WUFDYixjQUFjO1lBQ2QsaUJBQWlCO1lBQ2pCLGFBQWE7WUFDYix1QkFBdUI7WUFDdkIsc0JBQXNCO1lBQ3RCLHdCQUF3QjtZQUN4Qix3QkFBd0I7WUFDeEIsa0JBQWtCO1lBQ2xCLG1CQUFtQjtZQUNuQixlQUFlO1lBQ2YsZ0JBQWdCO1lBQ2hCLG1CQUFtQjtZQUNuQixvQkFBb0I7WUFDcEIscUJBQXFCO1lBQ3JCLGlCQUFpQjtZQUNqQixvQkFBb0I7WUFDcEIsa0JBQWtCO1lBQ2xCLGlCQUFpQjtZQUNqQixnQkFBZ0I7eUJBOUdwQjtDQW9IK0IsQUE3RS9CLElBNkUrQjtTQUFsQixjQUFjO3dGQUFkLGNBQWM7UUEzRXZCLHlCQUF5QjtRQUN6QixrQkFBa0I7UUFDbEIsd0JBQXdCO1FBQ3hCLGVBQWU7UUFDZix5QkFBeUI7UUFDekIsYUFBYTtRQUNiLHFCQUFxQjtRQUNyQixrQkFBa0I7UUFDbEIsdUJBQXVCO1FBQ3ZCLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsc0JBQXNCO1FBQ3RCLHFCQUFxQjtRQUNyQixhQUFhO1FBQ2IsY0FBYztRQUNkLGlCQUFpQjtRQUNqQixhQUFhO1FBQ2IsdUJBQXVCO1FBQ3ZCLHNCQUFzQjtRQUN0Qix3QkFBd0I7UUFDeEIsd0JBQXdCO1FBQ3hCLGtCQUFrQjtRQUNsQixtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixtQkFBbUI7UUFDbkIsb0JBQW9CO1FBQ3BCLHFCQUFxQjtRQUNyQixpQkFBaUI7UUFDakIsb0JBQW9CO1FBQ3BCLGtCQUFrQjtRQUNsQixpQkFBaUI7UUFDakIsZ0JBQWdCO1FBS2pCLHlCQUF5QjtRQUN4QixrQkFBa0I7UUFDbEIsd0JBQXdCO1FBQ3hCLGVBQWU7UUFDZix5QkFBeUI7UUFDekIsYUFBYTtRQUNiLHFCQUFxQjtRQUNyQixrQkFBa0I7UUFDbEIsdUJBQXVCO1FBQ3ZCLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsc0JBQXNCO1FBQ3RCLHFCQUFxQjtRQUNyQixhQUFhO1FBQ2IsY0FBYztRQUNkLGlCQUFpQjtRQUNqQixhQUFhO1FBQ2IsdUJBQXVCO1FBQ3ZCLHNCQUFzQjtRQUN0Qix3QkFBd0I7UUFDeEIsd0JBQXdCO1FBQ3hCLGtCQUFrQjtRQUNsQixtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixtQkFBbUI7UUFDbkIsb0JBQW9CO1FBQ3BCLHFCQUFxQjtRQUNyQixpQkFBaUI7UUFDakIsb0JBQW9CO1FBQ3BCLGtCQUFrQjtRQUNsQixpQkFBaUI7UUFDakIsZ0JBQWdCO2tEQU1QLGNBQWM7Y0E3RTFCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUU7b0JBQ1AseUJBQXlCO29CQUN6QixrQkFBa0I7b0JBQ2xCLHdCQUF3QjtvQkFDeEIsZUFBZTtvQkFDZix5QkFBeUI7b0JBQ3pCLGFBQWE7b0JBQ2IscUJBQXFCO29CQUNyQixrQkFBa0I7b0JBQ2xCLHVCQUF1QjtvQkFDdkIsbUJBQW1CO29CQUNuQixnQkFBZ0I7b0JBQ2hCLHNCQUFzQjtvQkFDdEIscUJBQXFCO29CQUNyQixhQUFhO29CQUNiLGNBQWM7b0JBQ2QsaUJBQWlCO29CQUNqQixhQUFhO29CQUNiLHVCQUF1QjtvQkFDdkIsc0JBQXNCO29CQUN0Qix3QkFBd0I7b0JBQ3hCLHdCQUF3QjtvQkFDeEIsa0JBQWtCO29CQUNsQixtQkFBbUI7b0JBQ25CLGVBQWU7b0JBQ2YsZ0JBQWdCO29CQUNoQixtQkFBbUI7b0JBQ25CLG9CQUFvQjtvQkFDcEIscUJBQXFCO29CQUNyQixpQkFBaUI7b0JBQ2pCLG9CQUFvQjtvQkFDcEIsa0JBQWtCO29CQUNsQixpQkFBaUI7b0JBQ2pCLGdCQUFnQjtpQkFHakI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNSLHlCQUF5QjtvQkFDeEIsa0JBQWtCO29CQUNsQix3QkFBd0I7b0JBQ3hCLGVBQWU7b0JBQ2YseUJBQXlCO29CQUN6QixhQUFhO29CQUNiLHFCQUFxQjtvQkFDckIsa0JBQWtCO29CQUNsQix1QkFBdUI7b0JBQ3ZCLG1CQUFtQjtvQkFDbkIsZ0JBQWdCO29CQUNoQixzQkFBc0I7b0JBQ3RCLHFCQUFxQjtvQkFDckIsYUFBYTtvQkFDYixjQUFjO29CQUNkLGlCQUFpQjtvQkFDakIsYUFBYTtvQkFDYix1QkFBdUI7b0JBQ3ZCLHNCQUFzQjtvQkFDdEIsd0JBQXdCO29CQUN4Qix3QkFBd0I7b0JBQ3hCLGtCQUFrQjtvQkFDbEIsbUJBQW1CO29CQUNuQixlQUFlO29CQUNmLGdCQUFnQjtvQkFDaEIsbUJBQW1CO29CQUNuQixvQkFBb0I7b0JBQ3BCLHFCQUFxQjtvQkFDckIsaUJBQWlCO29CQUNqQixvQkFBb0I7b0JBQ3BCLGtCQUFrQjtvQkFDbEIsaUJBQWlCO29CQUNqQixnQkFBZ0I7aUJBR2pCO2dCQUNELFlBQVksRUFBRSxFQUFFO2FBQ2pCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG4vLyBpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXHJcbi8vIGltcG9ydCB7IFNjcm9sbGluZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9zY3JvbGxpbmcnXHJcbi8vIGltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSdcclxuLy8gaW1wb3J0IHsgTWF0QmFkZ2VNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9iYWRnZSdcclxuLy8gaW1wb3J0IHsgTWF0Qm90dG9tU2hlZXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9ib3R0b20tc2hlZXQnXHJcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbidcclxuLy8gaW1wb3J0IHsgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uLXRvZ2dsZSdcclxuaW1wb3J0IHsgTWF0Q2FyZE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NhcmQnXHJcbi8vIGltcG9ydCB7IE1hdENoZWNrYm94TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hlY2tib3gnXHJcbi8vIGltcG9ydCB7IE1hdENoaXBzTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnXHJcbi8vIGltcG9ydCB7IE1hdE5hdGl2ZURhdGVNb2R1bGUsIE1hdFJpcHBsZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NvcmUnXHJcbi8vIGltcG9ydCB7IE1hdERhdGVwaWNrZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kYXRlcGlja2VyJ1xyXG4vLyBpbXBvcnQgeyBNYXREaWFsb2dNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnXHJcbmltcG9ydCB7IE1hdERpdmlkZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaXZpZGVyJ1xyXG4vLyBpbXBvcnQgeyBNYXRFeHBhbnNpb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9leHBhbnNpb24nXHJcbi8vIGltcG9ydCB7IE1hdEdyaWRMaXN0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZ3JpZC1saXN0J1xyXG5pbXBvcnQgeyBNYXRJY29uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbidcclxuXHJcbmltcG9ydCB7IE1hdElucHV0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaW5wdXQnXHJcbi8vIGltcG9ydCB7IE1hdExpc3RNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9saXN0J1xyXG5pbXBvcnQgeyBNYXRNZW51TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvbWVudSdcclxuLy8gaW1wb3J0IHsgTWF0UGFnaW5hdG9yTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJ1xyXG4vLyBpbXBvcnQgeyBNYXRQcm9ncmVzc0Jhck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3Byb2dyZXNzLWJhcidcclxuaW1wb3J0IHsgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3Mtc3Bpbm5lcidcclxuLy8gaW1wb3J0IHsgTWF0UmFkaW9Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9yYWRpbydcclxuaW1wb3J0IHsgTWF0U2VsZWN0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2VsZWN0J1xyXG5pbXBvcnQgeyBNYXRTaWRlbmF2TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2lkZW5hdidcclxuaW1wb3J0IHsgTWF0U2xpZGVUb2dnbGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbGlkZS10b2dnbGUnXHJcbi8vIGltcG9ydCB7IE1hdFNsaWRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NsaWRlcidcclxuLy8gaW1wb3J0IHsgTWF0U25hY2tCYXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXInXHJcbi8vIGltcG9ydCB7IE1hdFNvcnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0J1xyXG4vLyBpbXBvcnQgeyBNYXRTdGVwcGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc3RlcHBlcidcclxuLy8gaW1wb3J0IHsgTWF0VGFibGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSdcclxuLy8gaW1wb3J0IHsgTWF0VGFic01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYnMnXHJcbmltcG9ydCB7IE1hdFRvb2xiYXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90b29sYmFyJ1xyXG4vLyBpbXBvcnQgeyBNYXRUb29sdGlwTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdG9vbHRpcCdcclxuLy8gaW1wb3J0IHsgTWF0VHJlZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RyZWUnXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtcclxuICAgIC8vIE1hdEF1dG9jb21wbGV0ZU1vZHVsZSxcclxuICAgIC8vIE1hdEJhZGdlTW9kdWxlLFxyXG4gICAgLy8gTWF0Qm90dG9tU2hlZXRNb2R1bGUsXHJcbiAgICBNYXRCdXR0b25Nb2R1bGUsXHJcbiAgICAvLyBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXHJcbiAgICBNYXRDYXJkTW9kdWxlLFxyXG4gICAgLy8gTWF0Q2hlY2tib3hNb2R1bGUsXHJcbiAgICAvLyBNYXRDaGlwc01vZHVsZSxcclxuICAgIC8vIE1hdERhdGVwaWNrZXJNb2R1bGUsXHJcbiAgICAvLyBNYXREaWFsb2dNb2R1bGUsXHJcbiAgICBNYXREaXZpZGVyTW9kdWxlLFxyXG4gICAgLy8gTWF0RXhwYW5zaW9uTW9kdWxlLFxyXG4gICAgLy8gTWF0R3JpZExpc3RNb2R1bGUsXHJcbiAgICBNYXRJY29uTW9kdWxlLFxyXG4gICAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgICAvLyBNYXRMaXN0TW9kdWxlLFxyXG4gICAgTWF0TWVudU1vZHVsZSxcclxuICAgIC8vIE1hdE5hdGl2ZURhdGVNb2R1bGUsXHJcbiAgICAvLyBNYXRQYWdpbmF0b3JNb2R1bGUsXHJcbiAgICAvLyBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcclxuICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcclxuICAgIC8vIE1hdFJhZGlvTW9kdWxlLFxyXG4gICAgLy8gTWF0UmlwcGxlTW9kdWxlLFxyXG4gICAgTWF0U2VsZWN0TW9kdWxlLFxyXG4gICAgTWF0U2lkZW5hdk1vZHVsZSxcclxuICAgIC8vIE1hdFNsaWRlck1vZHVsZSxcclxuICAgIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxyXG4gICAgLy8gTWF0U25hY2tCYXJNb2R1bGUsXHJcbiAgICAvLyBNYXRTb3J0TW9kdWxlLFxyXG4gICAgLy8gTWF0U3RlcHBlck1vZHVsZSxcclxuICAgIC8vIE1hdFRhYmxlTW9kdWxlLFxyXG4gICAgLy8gTWF0VGFic01vZHVsZSxcclxuICAgIE1hdFRvb2xiYXJNb2R1bGUsXHJcbiAgICAvLyBNYXRUb29sdGlwTW9kdWxlLFxyXG4gICAgLy8gTWF0VHJlZU1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAvLyBNYXRBdXRvY29tcGxldGVNb2R1bGUsXHJcbiAgICAvLyBNYXRCYWRnZU1vZHVsZSxcclxuICAgIC8vIE1hdEJvdHRvbVNoZWV0TW9kdWxlLFxyXG4gICAgTWF0QnV0dG9uTW9kdWxlLFxyXG4gICAgLy8gTWF0QnV0dG9uVG9nZ2xlTW9kdWxlLFxyXG4gICAgTWF0Q2FyZE1vZHVsZSxcclxuICAgIC8vIE1hdENoZWNrYm94TW9kdWxlLFxyXG4gICAgLy8gTWF0Q2hpcHNNb2R1bGUsXHJcbiAgICAvLyBNYXREYXRlcGlja2VyTW9kdWxlLFxyXG4gICAgLy8gTWF0RGlhbG9nTW9kdWxlLFxyXG4gICAgTWF0RGl2aWRlck1vZHVsZSxcclxuICAgIC8vIE1hdEV4cGFuc2lvbk1vZHVsZSxcclxuICAgIC8vIE1hdEdyaWRMaXN0TW9kdWxlLFxyXG4gICAgTWF0SWNvbk1vZHVsZSxcclxuICAgIE1hdElucHV0TW9kdWxlLFxyXG4gICAgLy8gTWF0TGlzdE1vZHVsZSxcclxuICAgIE1hdE1lbnVNb2R1bGUsXHJcbiAgICAvLyBNYXROYXRpdmVEYXRlTW9kdWxlLFxyXG4gICAgLy8gTWF0UGFnaW5hdG9yTW9kdWxlLFxyXG4gICAgLy8gTWF0UHJvZ3Jlc3NCYXJNb2R1bGUsXHJcbiAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXHJcbiAgICAvLyBNYXRSYWRpb01vZHVsZSxcclxuICAgIC8vIE1hdFJpcHBsZU1vZHVsZSxcclxuICAgIE1hdFNlbGVjdE1vZHVsZSxcclxuICAgIE1hdFNpZGVuYXZNb2R1bGUsXHJcbiAgICAvLyBNYXRTbGlkZXJNb2R1bGUsXHJcbiAgICBNYXRTbGlkZVRvZ2dsZU1vZHVsZSxcclxuICAgIC8vIE1hdFNuYWNrQmFyTW9kdWxlLFxyXG4gICAgLy8gTWF0U29ydE1vZHVsZSxcclxuICAgIC8vIE1hdFN0ZXBwZXJNb2R1bGUsXHJcbiAgICAvLyBNYXRUYWJsZU1vZHVsZSxcclxuICAgIC8vIE1hdFRhYnNNb2R1bGUsXHJcbiAgICBNYXRUb29sYmFyTW9kdWxlLFxyXG4gICAgLy8gTWF0VG9vbHRpcE1vZHVsZSxcclxuICAgIC8vIE1hdFRyZWVNb2R1bGVcclxuICBdLFxyXG4gIGRlY2xhcmF0aW9uczogW11cclxufSlcclxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsTW9kdWxlIHsgfVxyXG5cclxuIl19