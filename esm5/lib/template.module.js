import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { SettingsModule } from './settings/settings.module';
import { CORE_CONFIG, AdminModule } from '@fof-angular/core';
import { LayoutModule } from './layout/layout.module';
import * as i0 from "@angular/core";
// import localeFr from '@angular/common/locales/fr'
// import { registerLocaleData } from '@angular/common'
// registerLocaleData(localeFr, 'fr')
var TemplateModule = /** @class */ (function () {
    function TemplateModule() {
    }
    TemplateModule.forRoot = function (fofConfig) {
        return {
            ngModule: TemplateModule,
            providers: [
                {
                    provide: CORE_CONFIG,
                    useValue: fofConfig
                }
            ]
        };
    };
    TemplateModule.ɵmod = i0.ɵɵdefineNgModule({ type: TemplateModule });
    TemplateModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TemplateModule_Factory(t) { return new (t || TemplateModule)(); }, imports: [[
                BrowserModule,
                MaterialModule,
                SettingsModule,
                LayoutModule,
                HttpClientModule,
                AdminModule
            ],
            BrowserAnimationsModule,
            LayoutModule] });
    return TemplateModule;
}());
export { TemplateModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TemplateModule, { imports: [BrowserModule,
        MaterialModule,
        SettingsModule,
        LayoutModule,
        HttpClientModule,
        AdminModule], exports: [BrowserAnimationsModule,
        LayoutModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TemplateModule, [{
        type: NgModule,
        args: [{
                declarations: [],
                imports: [
                    BrowserModule,
                    MaterialModule,
                    SettingsModule,
                    LayoutModule,
                    HttpClientModule,
                    AdminModule
                ],
                // providers: [
                //   {provide: LOCALE_ID, useValue: 'fr' },
                // ],
                exports: [
                    BrowserAnimationsModule,
                    LayoutModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi90ZW1wbGF0ZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBa0MsTUFBTSxlQUFlLENBQUE7QUFDeEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUE7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDJCQUEyQixDQUFBO0FBQ3pELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFBO0FBQzlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTyxtQkFBbUIsQ0FBQTtBQUNuRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUE7QUFDM0QsT0FBTyxFQUFjLFdBQVcsRUFBRSxXQUFXLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQTtBQUN4RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7O0FBQ3JELG9EQUFvRDtBQUNwRCx1REFBdUQ7QUFFdkQscUNBQXFDO0FBRXJDO0lBQUE7S0FnQ0M7SUFYUSxzQkFBTyxHQUFkLFVBQWUsU0FBcUI7UUFDbEMsT0FBTztZQUNMLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsV0FBVztvQkFDcEIsUUFBUSxFQUFFLFNBQVM7aUJBQ3BCO2FBQ0Y7U0FDRixDQUFBO0lBQ0gsQ0FBQztzREFYVSxjQUFjOytHQUFkLGNBQWMsa0JBaEJoQjtnQkFDUCxhQUFhO2dCQUNiLGNBQWM7Z0JBQ2QsY0FBYztnQkFDZCxZQUFZO2dCQUNaLGdCQUFnQjtnQkFDaEIsV0FBVzthQUNaO1lBS0MsdUJBQXVCO1lBQ3ZCLFlBQVk7eUJBOUJoQjtDQTZDQyxBQWhDRCxJQWdDQztTQVpZLGNBQWM7d0ZBQWQsY0FBYyxjQWZ2QixhQUFhO1FBQ2IsY0FBYztRQUNkLGNBQWM7UUFDZCxZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLFdBQVcsYUFNWCx1QkFBdUI7UUFDdkIsWUFBWTtrREFHSCxjQUFjO2NBcEIxQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLEVBRWI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLGFBQWE7b0JBQ2IsY0FBYztvQkFDZCxjQUFjO29CQUNkLFlBQVk7b0JBQ1osZ0JBQWdCO29CQUNoQixXQUFXO2lCQUNaO2dCQUNELGVBQWU7Z0JBQ2YsMkNBQTJDO2dCQUMzQyxLQUFLO2dCQUNMLE9BQU8sRUFBRTtvQkFDUCx1QkFBdUI7b0JBQ3ZCLFlBQVk7aUJBQ2I7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzLCBMT0NBTEVfSUQgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJ1xuaW1wb3J0IHsgQnJvd3Nlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInXG5pbXBvcnQgeyBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXIvYW5pbWF0aW9ucydcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gIGZyb20gJy4vbWF0ZXJpYWwubW9kdWxlJ1xuaW1wb3J0IHsgU2V0dGluZ3NNb2R1bGUgfSBmcm9tICcuL3NldHRpbmdzL3NldHRpbmdzLm1vZHVsZSdcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHLCBBZG1pbk1vZHVsZSB9IGZyb20gJ0Bmb2YtYW5ndWxhci9jb3JlJ1xuaW1wb3J0IHsgTGF5b3V0TW9kdWxlIH0gZnJvbSAnLi9sYXlvdXQvbGF5b3V0Lm1vZHVsZSdcbi8vIGltcG9ydCBsb2NhbGVGciBmcm9tICdAYW5ndWxhci9jb21tb24vbG9jYWxlcy9mcidcbi8vIGltcG9ydCB7IHJlZ2lzdGVyTG9jYWxlRGF0YSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcblxuLy8gcmVnaXN0ZXJMb2NhbGVEYXRhKGxvY2FsZUZyLCAnZnInKVxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIEJyb3dzZXJNb2R1bGUsXG4gICAgTWF0ZXJpYWxNb2R1bGUsICAgIFxuICAgIFNldHRpbmdzTW9kdWxlLFxuICAgIExheW91dE1vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIEFkbWluTW9kdWxlXG4gIF0sXG4gIC8vIHByb3ZpZGVyczogW1xuICAvLyAgIHtwcm92aWRlOiBMT0NBTEVfSUQsIHVzZVZhbHVlOiAnZnInIH0sXG4gIC8vIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSwgICAgXG4gICAgTGF5b3V0TW9kdWxlICAgIFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFRlbXBsYXRlTW9kdWxlIHsgICBcbiAgc3RhdGljIGZvclJvb3QoZm9mQ29uZmlnOiBJZm9mQ29uZmlnKTogTW9kdWxlV2l0aFByb3ZpZGVyczxUZW1wbGF0ZU1vZHVsZT4ge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogVGVtcGxhdGVNb2R1bGUsICAgICAgXG4gICAgICBwcm92aWRlcnM6IFsgICAgICAgIFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQ09SRV9DT05GSUcsXG4gICAgICAgICAgdXNlVmFsdWU6IGZvZkNvbmZpZ1xuICAgICAgICB9XG4gICAgICBdXG4gICAgfVxuICB9XG59XG4iXX0=