import { Component } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/router";
var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent.ɵfac = function NotFoundComponent_Factory(t) { return new (t || NotFoundComponent)(); };
    NotFoundComponent.ɵcmp = i0.ɵɵdefineComponent({ type: NotFoundComponent, selectors: [["company-not-found"]], decls: 9, vars: 1, consts: [[1, "main"], ["mat-stroked-button", "", 3, "routerLink"]], template: function NotFoundComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "h3");
            i0.ɵɵtext(2, "Vous \u00EAtes perdu ?");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "p");
            i0.ɵɵtext(4, " Ne bougez pas de l\u00E0 o\u00F9 vous \u00EAtes, quelqu'un vient vous chercher !");
            i0.ɵɵelement(5, "br");
            i0.ɵɵtext(6, " Ou alors... ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "a", 1);
            i0.ɵɵtext(8, " Revenez \u00E0 l'accueil ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(7);
            i0.ɵɵproperty("routerLink", "/");
        } }, directives: [i1.MatAnchor, i2.RouterLinkWithHref], styles: ["[_nghost-%COMP%]{display:flex;align-items:center;justify-content:center;height:100%}"] });
    return NotFoundComponent;
}());
export { NotFoundComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(NotFoundComponent, [{
        type: Component,
        args: [{
                selector: 'company-not-found',
                templateUrl: './not-found.component.html',
                styleUrls: ['./not-found.component.scss']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90LWZvdW5kLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25vdC1mb3VuZC9ub3QtZm91bmQuY29tcG9uZW50LnRzIiwibGliL2xheW91dC9ub3QtZm91bmQvbm90LWZvdW5kLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7Ozs7QUFFbEQ7SUFPRTtJQUFnQixDQUFDO0lBRWpCLG9DQUFRLEdBQVI7SUFDQSxDQUFDO3NGQUxVLGlCQUFpQjswREFBakIsaUJBQWlCO1lDUDlCLDhCQUNFO1lBQUEsMEJBQUk7WUFBQSxzQ0FBaUI7WUFBQSxpQkFBSztZQUMxQix5QkFDRTtZQUFBLGlHQUFpRTtZQUFBLHFCQUNqRTtZQUFBLDZCQUNGO1lBQUEsaUJBQUk7WUFDSiw0QkFDRTtZQUFBLDBDQUNGO1lBQUEsaUJBQUk7WUFFTixpQkFBTTs7WUFKa0IsZUFBa0I7WUFBbEIsZ0NBQWtCOzs0QkROMUM7Q0FjQyxBQVpELElBWUM7U0FQWSxpQkFBaUI7a0RBQWpCLGlCQUFpQjtjQUw3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsV0FBVyxFQUFFLDRCQUE0QjtnQkFDekMsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7YUFDMUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb21wYW55LW5vdC1mb3VuZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9ub3QtZm91bmQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ub3QtZm91bmQuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBOb3RGb3VuZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iLCI8ZGl2IGNsYXNzPVwibWFpblwiPlxuICA8aDM+Vm91cyDDqnRlcyBwZXJkdSA/PC9oMz5cbiAgPHA+XG4gICAgTmUgYm91Z2V6IHBhcyBkZSBsw6Agb8O5IHZvdXMgw6p0ZXMsIHF1ZWxxdSd1biB2aWVudCB2b3VzIGNoZXJjaGVyICE8YnI+XG4gICAgT3UgYWxvcnMuLi5cbiAgPC9wPiAgXG4gIDxhIG1hdC1zdHJva2VkLWJ1dHRvbiBbcm91dGVyTGlua109XCInLydcIj5cbiAgICBSZXZlbmV6IMOgIGwnYWNjdWVpbFxuICA8L2E+XG5cbjwvZGl2PlxuIl19