import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
// import { UsersModule } from '@fof-angular/core'
var routes = [
    {
        path: 'collaborators',
        loadChildren: function () { return import('@fof-angular/core').then(function (m) { return m.UsersModule; }); }
    },
    { path: 'login', component: LoginComponent },
    { path: 'loading', component: LoadingPageComponent }
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: LayoutRoutingModule });
    LayoutRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function LayoutRoutingModule_Factory(t) { return new (t || LayoutRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
            RouterModule] });
    return LayoutRoutingModule;
}());
export { LayoutRoutingModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(LayoutRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LayoutRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbGF5b3V0LXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFVLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQ3RELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQTs7O0FBQzVFLGtEQUFrRDtBQUVsRCxJQUFNLE1BQU0sR0FBVztJQUNyQjtRQUNFLElBQUksRUFBRSxlQUFlO1FBQ3JCLFlBQVksRUFBRSxjQUFNLE9BQUEsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFdBQVcsRUFBYixDQUFhLENBQUMsRUFBcEQsQ0FBb0Q7S0FDekU7SUFDRCxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRTtJQUM1QyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLG9CQUFvQixFQUFFO0NBQ3JELENBQUE7QUFHRDtJQUFBO0tBSW9DOzJEQUF2QixtQkFBbUI7eUhBQW5CLG1CQUFtQixrQkFIckIsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlCLFlBQVk7OEJBbEJ4QjtDQW9Cb0MsQUFKcEMsSUFJb0M7U0FBdkIsbUJBQW1CO3dGQUFuQixtQkFBbUIsMENBRnBCLFlBQVk7a0RBRVgsbUJBQW1CO2NBSi9CLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBSb3V0ZXMsIFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9sb2dpbi9sb2dpbi5jb21wb25lbnQnXG5pbXBvcnQgeyBMb2FkaW5nUGFnZUNvbXBvbmVudCB9IGZyb20gJy4vbG9hZGluZy1wYWdlL2xvYWRpbmctcGFnZS5jb21wb25lbnQnXG4vLyBpbXBvcnQgeyBVc2Vyc01vZHVsZSB9IGZyb20gJ0Bmb2YtYW5ndWxhci9jb3JlJ1xuXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcbiAge1xuICAgIHBhdGg6ICdjb2xsYWJvcmF0b3JzJyxcbiAgICBsb2FkQ2hpbGRyZW46ICgpID0+IGltcG9ydCgnQGZvZi1hbmd1bGFyL2NvcmUnKS50aGVuKG0gPT4gbS5Vc2Vyc01vZHVsZSlcbiAgfSxcbiAgeyBwYXRoOiAnbG9naW4nLCBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50IH0sXG4gIHsgcGF0aDogJ2xvYWRpbmcnLCBjb21wb25lbnQ6IExvYWRpbmdQYWdlQ29tcG9uZW50IH1cbl1cblxuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbUm91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxuICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBMYXlvdXRSb3V0aW5nTW9kdWxlIHsgfVxuIl19