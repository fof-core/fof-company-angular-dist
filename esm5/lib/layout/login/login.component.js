import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@fof-angular/core";
import * as i2 from "@angular/router";
import * as i3 from "@angular/material/card";
import * as i4 from "@angular/common";
import * as i5 from "@angular/forms";
import * as i6 from "@angular/material/form-field";
import * as i7 from "@angular/material/input";
import * as i8 from "@angular/material/button";
import * as i9 from "@angular/material/progress-spinner";
function LoginComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelement(1, "mat-spinner", 11);
    i0.ɵɵelementEnd();
} }
function LoginComponent_p_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "p", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r19 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r19.uiVar.error, " ");
} }
function LoginComponent_p_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "p", 13);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r20 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r20.uiVar.message, " ");
} }
var LoginComponent = /** @class */ (function () {
    function LoginComponent(foFAuthService, activatedRoute, router, fofNotificationService, fofErrorService) {
        var _this = this;
        this.foFAuthService = foFAuthService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            returnUrl: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            form: new FormGroup({
                username: new FormControl('superuser@fof.com'),
                password: new FormControl('changeMe')
            }),
            error: undefined,
            message: undefined,
            loading: false
        };
        // All actions shared with UI 
        this.uiAction = {
            submit: function () {
                if (!_this.uiVar.form.valid) {
                    return;
                }
                var username = _this.uiVar.form.value.username;
                var password = _this.uiVar.form.value.password;
                _this.uiVar.loading = true;
                _this.foFAuthService.login(username, password)
                    .toPromise()
                    .then(function (data) {
                    _this.router.navigate([_this.priVar.returnUrl]);
                })
                    .catch(function (reason) {
                    // this.uiVar.error = error
                    _this.fofErrorService.errorManage(reason);
                    _this.uiVar.loading = false;
                });
            }
        };
        this.priVar.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
        // try to get the user identity with the cookie
        this.uiVar.message = "Nous cherchons si vous nous connaissons...";
        this.foFAuthService.refreshByCookie()
            .toPromise()
            .then(function (user) {
            if (user) {
                _this.fofNotificationService.info("Bonjour " + user.firstName + " !");
                _this.router.navigate([_this.priVar.returnUrl]);
            }
        })
            .catch(function (reason) {
            // this.uiVar.error = reason.message
            _this.fofErrorService.errorManage(reason);
            _this.uiVar.loading = false;
        })
            .finally(function () { _this.uiVar.message = 'Veuillez vous authentifier'; });
    }
    // Angular events  
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.ngOnDestroy = function () {
    };
    LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(i0.ɵɵdirectiveInject(i1.FoFAuthService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i1.FofNotificationService), i0.ɵɵdirectiveInject(i1.FofErrorService)); };
    LoginComponent.ɵcmp = i0.ɵɵdefineComponent({ type: LoginComponent, selectors: [["company-login"]], decls: 19, vars: 4, consts: [[1, "background"], [1, "main"], ["class", "fof-loading", 4, "ngIf"], [3, "formGroup", "ngSubmit"], ["type", "text", "matInput", "", "placeholder", "Username", "formControlName", "username"], ["type", "password", "matInput", "", "placeholder", "Password", "formControlName", "password"], ["class", "error", 4, "ngIf"], ["class", "message", 4, "ngIf"], [1, "button"], ["type", "submit", "mat-button", ""], [1, "fof-loading"], ["diameter", "30"], [1, "error"], [1, "message"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "div", 1);
            i0.ɵɵelementStart(2, "mat-card");
            i0.ɵɵtemplate(3, LoginComponent_div_3_Template, 2, 0, "div", 2);
            i0.ɵɵelementStart(4, "mat-card-title");
            i0.ɵɵtext(5, " Login ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "mat-card-content");
            i0.ɵɵelementStart(7, "form", 3);
            i0.ɵɵlistener("ngSubmit", function LoginComponent_Template_form_ngSubmit_7_listener() { return ctx.uiAction.submit(); });
            i0.ɵɵelementStart(8, "p");
            i0.ɵɵelementStart(9, "mat-form-field");
            i0.ɵɵelement(10, "input", 4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "p");
            i0.ɵɵelementStart(12, "mat-form-field");
            i0.ɵɵelement(13, "input", 5);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(14, LoginComponent_p_14_Template, 2, 1, "p", 6);
            i0.ɵɵtemplate(15, LoginComponent_p_15_Template, 2, 1, "p", 7);
            i0.ɵɵelementStart(16, "div", 8);
            i0.ɵɵelementStart(17, "button", 9);
            i0.ɵɵtext(18, "Login");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("formGroup", ctx.uiVar.form);
            i0.ɵɵadvance(7);
            i0.ɵɵproperty("ngIf", ctx.uiVar.error);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.message);
        } }, directives: [i3.MatCard, i4.NgIf, i3.MatCardTitle, i3.MatCardContent, i5.ɵangular_packages_forms_forms_y, i5.NgControlStatusGroup, i5.FormGroupDirective, i6.MatFormField, i7.MatInput, i5.DefaultValueAccessor, i5.NgControlStatus, i5.FormControlName, i8.MatButton, i9.MatSpinner], styles: [".background[_ngcontent-%COMP%]{position:absolute;top:0;left:0;right:0;bottom:0;z-index:1000}.main[_ngcontent-%COMP%]{display:flex;justify-content:center;margin:100px 0}.main[_ngcontent-%COMP%]   .fof-loading[_ngcontent-%COMP%]{display:flex;justify-content:center;align-items:center;position:absolute;top:0;left:0;width:100%;height:100%;z-index:2}.main[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%;min-width:300px}.main[_ngcontent-%COMP%]   mat-card-content[_ngcontent-%COMP%], .main[_ngcontent-%COMP%]   mat-card-title[_ngcontent-%COMP%]{display:flex;justify-content:center}.main[_ngcontent-%COMP%]   .error[_ngcontent-%COMP%], .main[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]{padding:16px;width:300px}.main[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]{display:flex;justify-content:flex-end}"] });
    return LoginComponent;
}());
export { LoginComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LoginComponent, [{
        type: Component,
        args: [{
                selector: 'company-login',
                templateUrl: './login.component.html',
                styleUrls: ['./login.component.scss']
            }]
    }], function () { return [{ type: i1.FoFAuthService }, { type: i2.ActivatedRoute }, { type: i2.Router }, { type: i1.FofNotificationService }, { type: i1.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNvbXBhbnktYW5ndWxhci90ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbG9naW4vbG9naW4uY29tcG9uZW50LnRzIiwibGliL2xheW91dC9sb2dpbi9sb2dpbi5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFBO0FBQ2pELE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7Ozs7Ozs7Ozs7OztJQ0VqRCwrQkFDRTtJQUFBLGtDQUF1QztJQUN6QyxpQkFBTTs7O0lBZ0JGLDZCQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFJOzs7SUFERixlQUNGO0lBREUsb0RBQ0Y7OztJQUNBLDZCQUNFO0lBQUEsWUFDRjtJQUFBLGlCQUFJOzs7SUFERixlQUNGO0lBREUsc0RBQ0Y7O0FEckJWO0lBT0Usd0JBQ1UsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsTUFBYyxFQUNkLHNCQUE4QyxFQUM5QyxlQUFnQztRQUwxQyxpQkEwQkQ7UUF6QlcsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUF3QjFDLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixTQUFTLEVBQVUsU0FBUztTQUM3QixDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLElBQUksRUFBYyxJQUFJLFNBQVMsQ0FBQztnQkFDOUIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLG1CQUFtQixDQUFDO2dCQUM5QyxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3RDLENBQUM7WUFDRixLQUFLLEVBQVUsU0FBUztZQUN4QixPQUFPLEVBQVUsU0FBUztZQUMxQixPQUFPLEVBQUUsS0FBSztTQUNmLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLE1BQU0sRUFBQztnQkFFTCxJQUFJLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUMxQixPQUFNO2lCQUNQO2dCQUVELElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUE7Z0JBQy9DLElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUE7Z0JBRS9DLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQTtnQkFDekIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQztxQkFDNUMsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxVQUFBLElBQUk7b0JBQ1IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUE7Z0JBQy9DLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNO29CQUNYLDJCQUEyQjtvQkFDM0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQ3hDLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTtnQkFDNUIsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1NBQ0YsQ0FBQTtRQTdEQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxDQUFBO1FBRXBGLCtDQUErQztRQUMvQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyw0Q0FBNEMsQ0FBQTtRQUNqRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTthQUNwQyxTQUFTLEVBQUU7YUFDWCxJQUFJLENBQUMsVUFBQyxJQUFTO1lBQ2QsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFXLElBQUksQ0FBQyxTQUFTLE9BQUksQ0FBQyxDQUFBO2dCQUMvRCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQTthQUM5QztRQUNILENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFBLE1BQU07WUFDWCxvQ0FBb0M7WUFDcEMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7WUFDeEMsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO1FBQzVCLENBQUMsQ0FBQzthQUNELE9BQU8sQ0FBQyxjQUFRLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLDRCQUE0QixDQUFBLENBQUEsQ0FBQyxDQUFDLENBQUE7SUFDeEUsQ0FBQztJQTRDQyxtQkFBbUI7SUFDbkIsaUNBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCxvQ0FBVyxHQUFYO0lBQ0EsQ0FBQztnRkE3RVUsY0FBYzt1REFBZCxjQUFjO1lDVjNCLDhCQUNFO1lBQUEsOEJBQ0U7WUFBQSxnQ0FDRTtZQUFBLCtEQUNFO1lBRUYsc0NBQ0U7WUFBQSx1QkFDRjtZQUFBLGlCQUFpQjtZQUNqQix3Q0FDRTtZQUFBLCtCQUNFO1lBRDZCLCtGQUFZLHFCQUFpQixJQUFDO1lBQzNELHlCQUNFO1lBQUEsc0NBQ0U7WUFBQSw0QkFDRjtZQUFBLGlCQUFpQjtZQUNuQixpQkFBSTtZQUNKLDBCQUNFO1lBQUEsdUNBQ0U7WUFBQSw0QkFDRjtZQUFBLGlCQUFpQjtZQUNuQixpQkFBSTtZQUNKLDZEQUNFO1lBRUYsNkRBQ0U7WUFFRiwrQkFDRTtZQUFBLGtDQUFpQztZQUFBLHNCQUFLO1lBQUEsaUJBQVM7WUFDakQsaUJBQU07WUFDUixpQkFBTztZQUNULGlCQUFtQjtZQUNyQixpQkFBVztZQUNiLGlCQUFNO1lBQ1IsaUJBQU07O1lBL0JLLGVBQXFCO1lBQXJCLHdDQUFxQjtZQU9sQixlQUF3QjtZQUF4QiwwQ0FBd0I7WUFXekIsZUFBbUI7WUFBbkIsc0NBQW1CO1lBR25CLGVBQXFCO1lBQXJCLHdDQUFxQjs7eUJEeEJsQztDQXdGQyxBQW5GRCxJQW1GQztTQTlFWSxjQUFjO2tEQUFkLGNBQWM7Y0FMMUIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxlQUFlO2dCQUN6QixXQUFXLEVBQUUsd0JBQXdCO2dCQUNyQyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQzthQUN0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3JtcydcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG5pbXBvcnQgeyBGb0ZBdXRoU2VydmljZSwgRm9mTm90aWZpY2F0aW9uU2VydmljZSwgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnQGZvZi1hbmd1bGFyL2NvcmUnXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NvbXBhbnktbG9naW4nLFxuICB0ZW1wbGF0ZVVybDogJy4vbG9naW4uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9sb2dpbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZvRkF1dGhTZXJ2aWNlOiBGb0ZBdXRoU2VydmljZSxcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcbiAgICBwcml2YXRlIGZvZkVycm9yU2VydmljZTogRm9mRXJyb3JTZXJ2aWNlXG4gICkgeyBcblxuICAgIHRoaXMucHJpVmFyLnJldHVyblVybCA9IHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXNbJ3JldHVyblVybCddIHx8ICcvJ1xuXG4gICAgLy8gdHJ5IHRvIGdldCB0aGUgdXNlciBpZGVudGl0eSB3aXRoIHRoZSBjb29raWVcbiAgICB0aGlzLnVpVmFyLm1lc3NhZ2UgPSBcIk5vdXMgY2hlcmNob25zIHNpIHZvdXMgbm91cyBjb25uYWlzc29ucy4uLlwiICAgIFxuICAgIHRoaXMuZm9GQXV0aFNlcnZpY2UucmVmcmVzaEJ5Q29va2llKClcbiAgICAudG9Qcm9taXNlKClcbiAgICAudGhlbigodXNlcjogYW55KSA9PiB7ICAgICAgXG4gICAgICBpZiAodXNlcikge1xuICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbyhgQm9uam91ciAke3VzZXIuZmlyc3ROYW1lfSAhYClcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucHJpVmFyLnJldHVyblVybF0pXG4gICAgICB9ICAgICAgXG4gICAgfSlcbiAgICAuY2F0Y2gocmVhc29uID0+IHsgICAgICBcbiAgICAgIC8vIHRoaXMudWlWYXIuZXJyb3IgPSByZWFzb24ubWVzc2FnZVxuICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxuICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gZmFsc2VcbiAgICB9KVxuICAgIC5maW5hbGx5KCgpID0+IHsgdGhpcy51aVZhci5tZXNzYWdlID0gJ1ZldWlsbGV6IHZvdXMgYXV0aGVudGlmaWVyJ30pXG59XG4gXG5cbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXG4gIHByaXZhdGUgcHJpVmFyID0ge1xuICAgIHJldHVyblVybDogPHN0cmluZz51bmRlZmluZWQgICAgXG4gIH1cbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XG4gIH1cbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcbiAgcHVibGljIHVpVmFyID0ge1xuICAgIGZvcm06IDxGb3JtR3JvdXA+IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgdXNlcm5hbWU6IG5ldyBGb3JtQ29udHJvbCgnc3VwZXJ1c2VyQGZvZi5jb20nKSxcbiAgICAgIHBhc3N3b3JkOiBuZXcgRm9ybUNvbnRyb2woJ2NoYW5nZU1lJylcbiAgICB9KSxcbiAgICBlcnJvcjogPHN0cmluZz51bmRlZmluZWQsXG4gICAgbWVzc2FnZTogPHN0cmluZz51bmRlZmluZWQsXG4gICAgbG9hZGluZzogZmFsc2VcbiAgfVxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcbiAgcHVibGljIHVpQWN0aW9uID0ge1xuICAgIHN1Ym1pdDooKSA9PiB7XG5cbiAgICAgIGlmICghdGhpcy51aVZhci5mb3JtLnZhbGlkKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCB1c2VybmFtZSA9IHRoaXMudWlWYXIuZm9ybS52YWx1ZS51c2VybmFtZVxuICAgICAgY29uc3QgcGFzc3dvcmQgPSB0aGlzLnVpVmFyLmZvcm0udmFsdWUucGFzc3dvcmRcblxuICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gdHJ1ZVxuICAgICAgdGhpcy5mb0ZBdXRoU2VydmljZS5sb2dpbih1c2VybmFtZSwgcGFzc3dvcmQpXG4gICAgICAudG9Qcm9taXNlKClcbiAgICAgIC50aGVuKGRhdGEgPT4ge1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5wcmlWYXIucmV0dXJuVXJsXSlcbiAgICAgIH0pICAgICAgICAgXG4gICAgICAuY2F0Y2gocmVhc29uID0+IHsgICAgICAgIFxuICAgICAgICAvLyB0aGlzLnVpVmFyLmVycm9yID0gZXJyb3JcbiAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxuICAgICAgICB0aGlzLnVpVmFyLmxvYWRpbmcgPSBmYWxzZVxuICAgICAgfSkgICAgICAgICAgICAgXG4gICAgfVxuICB9XG4gIC8vIEFuZ3VsYXIgZXZlbnRzICBcbiAgbmdPbkluaXQoKSB7ICAgXG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHsgICAgXG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJiYWNrZ3JvdW5kXCI+XG4gIDxkaXYgY2xhc3M9XCJtYWluXCI+XG4gICAgPG1hdC1jYXJkPlxuICAgICAgPGRpdiAqbmdJZj1cInVpVmFyLmxvYWRpbmdcIiBjbGFzcz1cImZvZi1sb2FkaW5nXCI+XG4gICAgICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0zMD48L21hdC1zcGlubmVyPlxuICAgICAgPC9kaXY+XG4gICAgICA8bWF0LWNhcmQtdGl0bGU+XG4gICAgICAgIExvZ2luXG4gICAgICA8L21hdC1jYXJkLXRpdGxlPlxuICAgICAgPG1hdC1jYXJkLWNvbnRlbnQ+XG4gICAgICAgIDxmb3JtIFtmb3JtR3JvdXBdPVwidWlWYXIuZm9ybVwiIChuZ1N1Ym1pdCk9XCJ1aUFjdGlvbi5zdWJtaXQoKVwiPlxuICAgICAgICAgIDxwPlxuICAgICAgICAgICAgPG1hdC1mb3JtLWZpZWxkPlxuICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBtYXRJbnB1dCBwbGFjZWhvbGRlcj1cIlVzZXJuYW1lXCIgZm9ybUNvbnRyb2xOYW1lPVwidXNlcm5hbWVcIj5cbiAgICAgICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG4gICAgICAgICAgPC9wPiAgICBcbiAgICAgICAgICA8cD5cbiAgICAgICAgICAgIDxtYXQtZm9ybS1maWVsZD5cbiAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJwYXNzd29yZFwiIG1hdElucHV0IHBsYWNlaG9sZGVyPVwiUGFzc3dvcmRcIiBmb3JtQ29udHJvbE5hbWU9XCJwYXNzd29yZFwiPlxuICAgICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cbiAgICAgICAgICA8L3A+ICAgIFxuICAgICAgICAgIDxwICpuZ0lmPVwidWlWYXIuZXJyb3JcIiBjbGFzcz1cImVycm9yXCI+XG4gICAgICAgICAgICB7eyB1aVZhci5lcnJvciB9fVxuICAgICAgICAgIDwvcD4gICAgXG4gICAgICAgICAgPHAgKm5nSWY9XCJ1aVZhci5tZXNzYWdlXCIgY2xhc3M9XCJtZXNzYWdlXCI+XG4gICAgICAgICAgICB7eyB1aVZhci5tZXNzYWdlIH19XG4gICAgICAgICAgPC9wPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJidXR0b25cIj5cbiAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIG1hdC1idXR0b24+TG9naW48L2J1dHRvbj5cbiAgICAgICAgICA8L2Rpdj4gICAgXG4gICAgICAgIDwvZm9ybT5cbiAgICAgIDwvbWF0LWNhcmQtY29udGVudD5cbiAgICA8L21hdC1jYXJkPlxuICA8L2Rpdj5cbjwvZGl2PlxuXG5cblxuXG4iXX0=