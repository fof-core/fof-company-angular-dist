import { Component } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@fof-angular/core";
import * as i2 from "@angular/router";
var LoadingPageComponent = /** @class */ (function () {
    function LoadingPageComponent(foFAuthService, activatedRoute, router, fofNotificationService, fofErrorService) {
        var _this = this;
        this.foFAuthService = foFAuthService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            returnUrl: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            message: "Veuillez patienter,<br>\n                      Nous v\u00E9rifions que vous nous connaissons..."
        };
        // All actions shared with UI 
        this.uiAction = {};
        this.priVar.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
        // try to get the user identity with the cookie    
        this.foFAuthService.refreshByCookie()
            .toPromise()
            .then(function (user) {
            if (user) {
                return;
            }
            else {
                return _this.foFAuthService.loginKerberos();
            }
        })
            .finally(function () {
            _this.router.navigate([_this.priVar.returnUrl]);
        })
            .catch(function (reason) {
            if (reason.status == 401) {
                var usereMail = reason.user.email;
                _this.uiVar.message = "Vous n'\u00EAtes pas authoris\u00E9 \u00E0 vous connecter \u00E0 cette application<br>\n          Utilisateur: " + usereMail;
            }
            else {
                _this.fofErrorService.errorManage(reason);
            }
        });
    }
    // Angular events
    LoadingPageComponent.prototype.ngOnInit = function () {
        // this.foFAuthService.loginKerberos()
        // .then(() => this.router.navigate([this.priVar.returnUrl]))
        // .catch((reason:iFofHttpException) => {
        //   if (reason.status==401) {
        //     let usereMail = reason.user.email
        //     this.uiVar.message = `Vous n'êtes pas authorisé à vous connecter à cette application<br>
        //       Utilisateur: ${usereMail}`
        //   } else {
        //     this.fofErrorService.errorManage(<any>reason)
        //   }      
        // })
    };
    LoadingPageComponent.ɵfac = function LoadingPageComponent_Factory(t) { return new (t || LoadingPageComponent)(i0.ɵɵdirectiveInject(i1.FoFAuthService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i1.FofNotificationService), i0.ɵɵdirectiveInject(i1.FofErrorService)); };
    LoadingPageComponent.ɵcmp = i0.ɵɵdefineComponent({ type: LoadingPageComponent, selectors: [["company-loading-page"]], decls: 2, vars: 1, consts: [[1, "background"], [1, "main", 3, "innerHTML"]], template: function LoadingPageComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelement(1, "div", 1);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("innerHTML", ctx.uiVar.message, i0.ɵɵsanitizeHtml);
        } }, styles: [".background[_ngcontent-%COMP%]{position:absolute;top:0;left:0;right:0;bottom:0;z-index:1000;opacity:.3}.main[_ngcontent-%COMP%]{display:flex;justify-content:center;margin:100px 0}"] });
    return LoadingPageComponent;
}());
export { LoadingPageComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(LoadingPageComponent, [{
        type: Component,
        args: [{
                selector: 'company-loading-page',
                templateUrl: './loading-page.component.html',
                styleUrls: ['./loading-page.component.scss']
            }]
    }], function () { return [{ type: i1.FoFAuthService }, { type: i2.ActivatedRoute }, { type: i2.Router }, { type: i1.FofNotificationService }, { type: i1.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy1wYWdlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bjb21wYW55LWFuZ3VsYXIvdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xvYWRpbmctcGFnZS9sb2FkaW5nLXBhZ2UuY29tcG9uZW50LnRzIiwibGliL2xheW91dC9sb2FkaW5nLXBhZ2UvbG9hZGluZy1wYWdlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUE7Ozs7QUFLakQ7SUFNRSw4QkFDVSxjQUE4QixFQUM5QixjQUE4QixFQUM5QixNQUFjLEVBQ2Qsc0JBQThDLEVBQzlDLGVBQWdDO1FBTDFDLGlCQStCQztRQTlCUyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQTRCMUMsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLFNBQVMsRUFBVSxTQUFTO1NBQzdCLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsT0FBTyxFQUFVLGlHQUM2QztTQUMvRCxDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRyxFQUNqQixDQUFBO1FBeENDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLENBQUE7UUFFcEYsbURBQW1EO1FBQ25ELElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFO2FBQ3BDLFNBQVMsRUFBRTthQUNYLElBQUksQ0FBQyxVQUFDLElBQVM7WUFDZCxJQUFJLElBQUksRUFBRTtnQkFDUixPQUFNO2FBQ1A7aUJBQU07Z0JBQ0wsT0FBTyxLQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFBO2FBQzNDO1FBQ0gsQ0FBQyxDQUFDO2FBQ0QsT0FBTyxDQUFDO1lBQ1AsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUE7UUFDL0MsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUEsTUFBTTtZQUNYLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBRSxHQUFHLEVBQUU7Z0JBQ3RCLElBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO2dCQUNqQyxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxvSEFDSixTQUFXLENBQUE7YUFDN0I7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQU0sTUFBTSxDQUFDLENBQUE7YUFDOUM7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFpQkQsaUJBQWlCO0lBQ2pCLHVDQUFRLEdBQVI7UUFDRSxzQ0FBc0M7UUFDdEMsNkRBQTZEO1FBQzdELHlDQUF5QztRQUN6Qyw4QkFBOEI7UUFDOUIsd0NBQXdDO1FBQ3hDLCtGQUErRjtRQUMvRixtQ0FBbUM7UUFDbkMsYUFBYTtRQUNiLG9EQUFvRDtRQUNwRCxZQUFZO1FBRVosS0FBSztJQUNQLENBQUM7NEZBL0RVLG9CQUFvQjs2REFBcEIsb0JBQW9CO1lDVmpDLDhCQUNFO1lBQUEseUJBRU07WUFDUixpQkFBTTs7WUFGRixlQUEyQjtZQUEzQixnRUFBMkI7OytCREYvQjtDQTBFQyxBQXJFRCxJQXFFQztTQWhFWSxvQkFBb0I7a0RBQXBCLG9CQUFvQjtjQUxoQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtnQkFDaEMsV0FBVyxFQUFFLCtCQUErQjtnQkFDNUMsU0FBUyxFQUFFLENBQUMsK0JBQStCLENBQUM7YUFDN0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXG5pbXBvcnQgeyBGb0ZBdXRoU2VydmljZSwgRm9mTm90aWZpY2F0aW9uU2VydmljZSwgXG4gIEZvZkVycm9yU2VydmljZSwgaUZvZkh0dHBFeGNlcHRpb24gfSBmcm9tICdAZm9mLWFuZ3VsYXIvY29yZSdcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NvbXBhbnktbG9hZGluZy1wYWdlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2xvYWRpbmctcGFnZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2xvYWRpbmctcGFnZS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIExvYWRpbmdQYWdlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBmb0ZBdXRoU2VydmljZTogRm9GQXV0aFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmb2ZFcnJvclNlcnZpY2U6IEZvZkVycm9yU2VydmljZVxuICApIHsgXG4gICAgdGhpcy5wcmlWYXIucmV0dXJuVXJsID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5xdWVyeVBhcmFtc1sncmV0dXJuVXJsJ10gfHwgJy8nXG5cbiAgICAvLyB0cnkgdG8gZ2V0IHRoZSB1c2VyIGlkZW50aXR5IHdpdGggdGhlIGNvb2tpZSAgICBcbiAgICB0aGlzLmZvRkF1dGhTZXJ2aWNlLnJlZnJlc2hCeUNvb2tpZSgpXG4gICAgLnRvUHJvbWlzZSgpXG4gICAgLnRoZW4oKHVzZXI6IGFueSkgPT4geyAgICAgIFxuICAgICAgaWYgKHVzZXIpIHsgICAgICAgIFxuICAgICAgICByZXR1cm5cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aGlzLmZvRkF1dGhTZXJ2aWNlLmxvZ2luS2VyYmVyb3MoKSAgICAgICAgXG4gICAgICB9XG4gICAgfSlcbiAgICAuZmluYWxseSgoKSA9PiB7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5wcmlWYXIucmV0dXJuVXJsXSlcbiAgICB9KVxuICAgIC5jYXRjaChyZWFzb24gPT4geyAgICBcbiAgICAgIGlmIChyZWFzb24uc3RhdHVzPT00MDEpIHtcbiAgICAgICAgbGV0IHVzZXJlTWFpbCA9IHJlYXNvbi51c2VyLmVtYWlsXG4gICAgICAgIHRoaXMudWlWYXIubWVzc2FnZSA9IGBWb3VzIG4nw6p0ZXMgcGFzIGF1dGhvcmlzw6kgw6Agdm91cyBjb25uZWN0ZXIgw6AgY2V0dGUgYXBwbGljYXRpb248YnI+XG4gICAgICAgICAgVXRpbGlzYXRldXI6ICR7dXNlcmVNYWlsfWBcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKDxhbnk+cmVhc29uKVxuICAgICAgfSAgICBcbiAgICB9KSAgICBcbiAgfVxuICBcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXG4gIHByaXZhdGUgcHJpVmFyID0ge1xuICAgIHJldHVyblVybDogPHN0cmluZz51bmRlZmluZWRcbiAgfVxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcbiAgfVxuICAvLyBBbGwgdmFyaWFibGVzIHNoYXJlZCB3aXRoIFVJIFxuICBwdWJsaWMgdWlWYXIgPSB7XG4gICAgbWVzc2FnZTogPHN0cmluZz5gVmV1aWxsZXogcGF0aWVudGVyLDxicj5cbiAgICAgICAgICAgICAgICAgICAgICBOb3VzIHbDqXJpZmlvbnMgcXVlIHZvdXMgbm91cyBjb25uYWlzc29ucy4uLmBcbiAgfVxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcbiAgcHVibGljIHVpQWN0aW9uID0ge1xuICB9XG4gIC8vIEFuZ3VsYXIgZXZlbnRzXG4gIG5nT25Jbml0KCkge1xuICAgIC8vIHRoaXMuZm9GQXV0aFNlcnZpY2UubG9naW5LZXJiZXJvcygpXG4gICAgLy8gLnRoZW4oKCkgPT4gdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucHJpVmFyLnJldHVyblVybF0pKVxuICAgIC8vIC5jYXRjaCgocmVhc29uOmlGb2ZIdHRwRXhjZXB0aW9uKSA9PiB7XG4gICAgLy8gICBpZiAocmVhc29uLnN0YXR1cz09NDAxKSB7XG4gICAgLy8gICAgIGxldCB1c2VyZU1haWwgPSByZWFzb24udXNlci5lbWFpbFxuICAgIC8vICAgICB0aGlzLnVpVmFyLm1lc3NhZ2UgPSBgVm91cyBuJ8OqdGVzIHBhcyBhdXRob3Jpc8OpIMOgIHZvdXMgY29ubmVjdGVyIMOgIGNldHRlIGFwcGxpY2F0aW9uPGJyPlxuICAgIC8vICAgICAgIFV0aWxpc2F0ZXVyOiAke3VzZXJlTWFpbH1gXG4gICAgLy8gICB9IGVsc2Uge1xuICAgIC8vICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZSg8YW55PnJlYXNvbilcbiAgICAvLyAgIH0gICAgICBcbiAgICAgIFxuICAgIC8vIH0pXG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJiYWNrZ3JvdW5kXCI+XG4gIDxkaXYgY2xhc3M9XCJtYWluXCJcbiAgICBbaW5uZXJIVE1MXT1cInVpVmFyLm1lc3NhZ2VcIj4gICAgICAgIFxuICA8L2Rpdj5cbjwvZGl2PiJdfQ==